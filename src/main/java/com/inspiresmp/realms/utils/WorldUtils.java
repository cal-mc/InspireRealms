package com.inspiresmp.realms.utils;

import com.inspiresmp.realms.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class WorldUtils {


    public static boolean loadWorld(String worldName) {
        for (World world : Bukkit.getWorlds()) {
            if (world.getName().equalsIgnoreCase(worldName)) {
                return false;
            }
        }

        Bukkit.createWorld(new WorldCreator(worldName));
        Main.instance.getLogger().log(Level.INFO, "World [" + worldName + "] loaded!");

        return true;
    }

    /**
     * Unloads a world from memory
     *
     * @return True: World was unloaded, False: World was not unloaded
     */
    public static boolean unloadWorld(World world) {
        if (world != null && isLoaded(world)) {
            Main.instance.getLogger().log(Level.INFO, "World [" + world.getName() + "] unloaded!");
            Bukkit.getServer().unloadWorld(world, true);
            return true;
        }

        return false;
    }

    /**
     * Is the world loaded in memory?
     *
     * @return True: World is loaded, False: World is not loaded
     */
    public static boolean isLoaded(World world) {
        return Bukkit.getWorlds().contains(world);
    }

    /**
     * Deletes a world from disk
     *
     * @param worldDir File worldDir to the world
     * @return True: World was deleted, False: World as not deleted
     */
    public static boolean deleteWorld(File worldDir) {
        if (worldDir.exists()) {
            if (worldDir.listFiles() == null) return false;
            File worldFiles[] = worldDir.listFiles();
            for (int i = 0; i < worldFiles.length; i++) {
                if (worldFiles[i].isDirectory()) {
                    deleteWorld(worldFiles[i]);
                } else {
                    worldFiles[i].delete();
                }
            }
        }
        return (worldDir.delete());
    }

    /**
     * Copy a world
     *
     * @param sourceWorld The source world
     * @param targetWorld The target world
     */
    public static void copyWorld(File sourceWorld, File targetWorld) {
        try {
            ArrayList<String> ignore = new ArrayList<>(Arrays.asList("uid.dat", "session.dat"));
            if (!ignore.contains(sourceWorld.getName())) {
                if (sourceWorld.isDirectory()) {
                    if (!targetWorld.exists())
                        targetWorld.mkdirs();
                    String files[] = sourceWorld.list();
                    for (String file : files) {
                        File srcFile = new File(sourceWorld, file);
                        File destFile = new File(targetWorld, file);
                        copyWorld(srcFile, destFile);
                    }
                } else {
                    InputStream in = new FileInputStream(sourceWorld);
                    OutputStream out = new FileOutputStream(targetWorld);
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = in.read(buffer)) > 0)
                        out.write(buffer, 0, length);
                    in.close();
                    out.close();
                }
            }
        } catch (IOException e) {
            Main.instance.getLogger().log(Level.WARNING, "Unable to copy world from " + sourceWorld.getPath() + " to " + targetWorld.getPath());
        }
    }

    public static void setWorldborder(CommandSender sender, int size) {
        Player player = null;
        if(sender instanceof Player){
            player = (Player) sender;
        }
        boolean wasOp = true;
        if (player != null && !player.isOp()) {
            player.setOp(true);
            wasOp = false;
        }

        Main.instance.getServer().dispatchCommand(sender, "worldborder set " + size);

        if (player != null && !wasOp) {
            player.setOp(false);
        }


    }

    public static Location getSpawnLocation(){
        FileConfiguration config = Main.instance.getConfig();
        Location spawnLocation = Bukkit.getWorlds().get(0).getSpawnLocation();
        if(config.get("spawnLocation") != null){
            spawnLocation = GameUtils.stringToLocation(config.getString("spawnLocation"));
        }
        return spawnLocation;
    }

    public static String getRawSpawnLocation(){
        return GameUtils.locationToString(getSpawnLocation());
    }

}
