package com.inspiresmp.realms.utils.messages;

/**
 * Crafted in heart of Wales by CaLxCyMru!
 */
public enum MessageType {
    CHAT, HOTBAR, TITLE, SUBTITLE
}
