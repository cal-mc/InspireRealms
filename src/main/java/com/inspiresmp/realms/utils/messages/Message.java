package com.inspiresmp.realms.utils.messages;

import net.minecraft.server.v1_8_R1.*;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.Collection;

/**
 * Crafted in heart of Wales by CaLxCyMru!
 */
public class Message {

    // The message to be sent
    private String message = null;

    // Title timing
    private int titleFadeIn = 5;
    private int titleLength = 60;
    private int titleFadeOut = 10;

    public Message(String message) {
        this.message = message;
    }

    /**
     * Sets the fade in time for the title
     *
     * @param titleFadeIn The new fade in time
     * @return The current instance of this class
     */
    public Message setTitleFadeIn(int titleFadeIn) {
        this.titleFadeIn = titleFadeIn;
        return this;
    }

    /**
     * Sets the time the title is displayed for
     *
     * @param titleLength The new length of the title
     * @return The current instance of this class
     */
    public Message setTitleLength(int titleLength) {
        this.titleLength = titleLength;
        return this;
    }

    /**
     * Sets the fade out time for the title
     *
     * @param titleFadeOut The new fade out time
     * @return The current instance of this class
     */
    public Message setTitleFadeOut(int titleFadeOut) {
        this.titleFadeOut = titleFadeOut;
        return this;
    }

    /**
     * Gets the title fade in time
     *
     * @return Fade in time
     */
    public int getTitleFadeIn() {
        return titleFadeIn;
    }

    /**
     * Gets the title length time
     *
     * @return Title length time
     */
    public int getTitleLength() {
        return titleLength;
    }

    /**
     * Get title fade out time
     *
     * @return Title fade out time
     */
    public int getTitleFadeOut() {
        return titleFadeOut;
    }

    /**
     * Gets the message without any formatting
     *
     * @return The message to be sent with no formatting
     */
    public String getMessage() {
        return this.message;
    }


    /**
     * Gets the message to be sent with colour codes
     *
     * @return The formatted message
     */
    public String getFormattedMessage() {
        return ChatColor.translateAlternateColorCodes('&', getMessage());
    }

    /**
     * Sends the message to a recipient
     *
     * @param recipient The person who will receive it
     */
    public void send(CommandSender recipient) {
        recipient.sendMessage(getFormattedMessage());
    }

    /**
     * Sends the message to a recipient
     *
     * @param recipient The person who will receive it
     * @param messageType The way in which to display the message
     */
    public void send(CommandSender recipient, MessageType messageType) {
        if (!(recipient instanceof Player)) {
            send(recipient);
            return;
        }
        CraftPlayer player = (CraftPlayer) recipient;
        IChatBaseComponent jsonMessage = ChatSerializer.a(MessageUtils.getJSON(getFormattedMessage()));
        switch (messageType) {
            case CHAT:
                send(recipient);
                return;
            case HOTBAR:
                IChatBaseComponent barMessage = ChatSerializer.a("{\"text\": \"" + getFormattedMessage() + "\"}");
                PacketPlayOutChat bar = new PacketPlayOutChat(barMessage, (byte) 2);
                player.getHandle().playerConnection.sendPacket(bar);
                return;
            case TITLE:
                PlayerConnection titleConnection = player.getHandle().playerConnection;
                PacketPlayOutTitle titlePacketPlayOutTimes = new PacketPlayOutTitle(EnumTitleAction.TIMES, null, getTitleFadeIn(), getTitleLength(), getTitleFadeOut());

                PacketPlayOutTitle packetPlayOutTitle = new PacketPlayOutTitle(EnumTitleAction.TITLE, jsonMessage);

                titleConnection.sendPacket(titlePacketPlayOutTimes);
                titleConnection.sendPacket(packetPlayOutTitle);
                return;
            case SUBTITLE:
                PlayerConnection subtitleConnection = player.getHandle().playerConnection;
                PacketPlayOutTitle subtitlePacketPlayOutTimes = new PacketPlayOutTitle(EnumTitleAction.TITLE, null, getTitleFadeIn(), getTitleLength(), getTitleFadeOut());
                subtitleConnection.sendPacket(subtitlePacketPlayOutTimes);

                PacketPlayOutTitle packetPlayOutSubTitle = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, jsonMessage);
                subtitleConnection.sendPacket(packetPlayOutSubTitle);
                return;
            default:
                send(recipient);
                break;
        }
    }

    /**
     * Sends the message to a multiple recipients
     *
     * @param recipients The person who will receive it
     */
    public void send(Collection<CommandSender> recipients) {
        for (CommandSender recipient : recipients) {
            send(recipient);
        }
    }

    /**
     * Sends the message to a multiple recipients
     *
     * @param recipients  The person who will receive it
     * @param messageType The way in which to display the message
     */
    public void send(Collection<CommandSender> recipients, MessageType messageType) {
        for (CommandSender recipient : recipients) {
            send(recipient, messageType);
        }
    }

}
