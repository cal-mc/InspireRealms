package com.inspiresmp.realms.utils;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.*;
import java.util.logging.Level;

public class SimpleScoreboard {

    private Scoreboard scoreboard;

    private String title;
    private Map<String, Integer> scores;
    private List<Team> teams;
    private Objective obj;

    public SimpleScoreboard(String title) {
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.title = formatString(title);
        this.scores = Maps.newLinkedHashMap();
        this.teams = Lists.newArrayList();
    }

    /**
     * Adds a blank line to the scoreboard
     *
     * @return Current instance
     */
    public SimpleScoreboard blankLine() {
        add(" ");
        return this;
    }


    /**
     * Gets the scoreboard
     *
     * @return Scoreboard
     */
    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    /**
     * Returns a list of strings that have said score
     *
     * @param index The score identifier you wish to search on
     * @return List of strings which have said score
     */
    public List<String> get(int index) {
        List<String> ls = new ArrayList<>();

        for (Map.Entry<String, Integer> entry : scores.entrySet()) {
            if (entry.getValue() != null && entry.getValue() == index) {
                ls.add(entry.getKey());
            }
        }

        return ls;
    }

    /**
     * Returns the index of the first occurrence of the specified string identifier
     *
     * @param text The String identifier you wish to get the index of
     * @return Index of said identifier
     */
    public int indexOf(String text) {
        return scores.get(formatString(text));
    }

    /**
     * Sets the first occurrence of the score with said index to the provided text
     *
     * @param index   The score identifier you wish to set
     * @param newText The text you wish to set it as
     * @return Current instance
     */
    public SimpleScoreboard set(int index, String newText) {
        newText = formatString(newText);
        String key = formatString(get(index).get(0));
        int position = indexOf(key);

        scores.remove(key);
        scores.put(newText, position);

        return this;
    }

    /**
     * Removes the first occurrence of the score with said index
     *
     * @param index The score identifier you wish to remove
     * @return Current instance
     */
    public SimpleScoreboard remove(int index) {
        return remove(get(index).get(0));
    }

    /**
     * Removes the first occurrence of the score with said identifier
     *
     * @param identifier The String identifier you wish to use
     * @return Current instance
     */
    public SimpleScoreboard remove(String identifier) {
        scores.remove(identifier);
        return this;
    }

    /**
     * Adds text to the scoreboard with a 'null' score
     *
     * @param text The text you wish to have the score as
     * @return Current instance
     */
    public SimpleScoreboard add(String text) {
        return add(text, null);
    }

    /**
     * Adds text to the scoreboard with specified score
     *
     * @param text  The text you wish to have the score as
     * @param score The score you wish to have the score have
     * @return Current instance
     */
    public SimpleScoreboard add(String text, Integer score) {
        text = formatString(text);
        Preconditions.checkArgument(text.length() < 48, "text cannot be over 48 characters in length");
        text = fixDuplicates(text);
        scores.put(text, score);

        return this;
    }

    private String fixDuplicates(String text) {
        while (scores.containsKey(text))
            text += "ยงr";
        if (text.length() > 48)
            text = text.substring(0, 47);
        return text;
    }

    private Map.Entry<Team, String> createTeam(String text) {
        String result = "";

        if (text.length() <= 16)
            return new AbstractMap.SimpleEntry<>(null, text);

        Team team = scoreboard.registerNewTeam("text-" + scoreboard.getTeams().size());
        Iterator<String> iterator = Splitter.fixedLength(16).split(text).iterator();

        team.setPrefix(iterator.next());
        result = iterator.next();

        if (text.length() > 32)
            team.setSuffix(iterator.next());

        teams.add(team);
        return new AbstractMap.SimpleEntry<>(team, result);
    }

    /**
     * Builds the scoreboard
     *
     * @return Current instance
     */
    public SimpleScoreboard build() {
        if (obj == null) {
            obj = scoreboard.registerNewObjective((title.length() > 16) ? title.substring(0, 15) : title, "dummy");
            obj.setDisplayName(title);
            obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        }

        int index = scores.size();

        for (Map.Entry<String, Integer> text : scores.entrySet()) {
            Map.Entry<Team, String> team = createTeam(text.getKey());
            Integer score = (text.getValue() != null) ? text.getValue() : index;

            OfflinePlayer player = null;

            if (team.getValue() == null || team.getValue().equalsIgnoreCase(" ")) {
                player = Bukkit.getOfflinePlayer(ChatColor.RED + " ");
            } else {
                player = Bukkit.getOfflinePlayer(team.getValue());
            }

            if (team.getKey() != null)
                team.getKey().addPlayer(player);

            obj.getScore(team.getValue()).setScore(score);
            index -= 1;
        }

        return this;
    }

    /**
     * Resets score and unregister all teams
     *
     * @return Instance of SimpleScoreboard class
     */
    public SimpleScoreboard reset() {
        title = null;
        scores.clear();
        for (Team t : teams) {
            t.unregister();
        }
        teams.clear();
        return this;
    }

    /**
     * Sets the players scoreboards to the current one
     *
     * @param players Players you wish to set
     * @return Instance of SimpleScoreboard class
     */
    public SimpleScoreboard send(Player... players) {
        for (Player p : players) {
            p.setScoreboard(scoreboard);
        }
        return this;
    }

    /**
     * Unregister the current objective
     *
     * @return Current instance
     */
    public SimpleScoreboard unregister() {
        obj.unregister();
        obj = null;

        return this;
    }

    /**
     * Gets the current title of the scoreboard
     *
     * @return Current title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title for the scoreboard
     *
     * @param newTitle The new title
     */
    public void setTitle(String newTitle) {
        title = newTitle;
    }

    /**
     * Update a scoreboard player with a new score
     *
     * @param id    The player/text to be updated
     * @param score The new score of the player/text
     * @return Instance of the SimpleScoreboard class
     */
    public SimpleScoreboard update(String id, Integer score) {
        try {
            if (scores.containsKey(formatString(id))) {
                for (Team t : teams) {
                    if (t.getName().equalsIgnoreCase(formatString(id))) {
                        t.unregister();
                    }
                    scores.put(formatString(id), score);
                }

                Map.Entry<Team, String> team = createTeam(formatString(id));
                OfflinePlayer player = Bukkit.getOfflinePlayer(team.getValue());
                if (team.getKey() != null)
                    team.getKey().addPlayer(player);
                obj.getScore(player).setScore(score);

            }
        } catch (Exception e) {
            Bukkit.getLogger().log(Level.WARNING, "Error updating scoreboard: ", id);
        }
        return this;
    }


    /**
     * Formats a string to use minecraft colour codes
     *
     * @param toFormat The string to be formatted
     * @return The formatted string
     */
    public String formatString(String toFormat) {
        if (toFormat.contains("&")) {
            toFormat = ChatColor.translateAlternateColorCodes('&', toFormat);
        }
        return toFormat;
    }


}
