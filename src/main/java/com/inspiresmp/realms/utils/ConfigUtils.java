package com.inspiresmp.realms.utils;

import com.inspiresmp.realms.Main;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class ConfigUtils {

    // File path
    private String filePath = null;

    public ConfigUtils(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Saves an embedded resource in the project to disk
     *
     * @param resourceName The resource to be saved to disk
     * @param file         Which file the resource should be saved to on the disk
     * @return True: Write file successfully, False: Didn't write file
     */
    public static boolean writeResourceToFile(String resourceName, File file) {
              boolean result = false;

        file.getParentFile().mkdir();
        // 1Kb buffer
        byte[] buffer = new byte[1024];
        int byteCount = 0;

        InputStream inputStream = null;
        OutputStream outputStream = null;
        Logger logger = Main.instance.getLogger();

        try {
            inputStream = Main.instance.getResource(resourceName);
            outputStream = new FileOutputStream(file);

            while ((byteCount = inputStream.read(buffer)) >= 0) {
                outputStream.write(buffer, 0, byteCount);
            }

            // Report success
            result = true;
        } catch (final IOException e) {
            logger.log(Level.WARNING, "Failure on saving the embedded resource " + resourceName + " to the file " + file.getAbsolutePath(), e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (final IOException e) {
                    logger.log(Level.WARNING, "Problem closing an input stream while reading data from the embedded resource " + resourceName, e);
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (final IOException e) {
                    logger.log(Level.WARNING, "Problem closing the output stream while writing the file " + file.getAbsolutePath(), e);
                }
            }
        }

        return result;
    }

    /**
     * Check if the config file exists
     *
     * @return True: File exists, False: File does not exist
     */
    public boolean fileExists() {
        return new File(filePath).exists();
    }

    /**
     * Creates a config file if one does not exist using a embedded resource
     */
    public void createConfig(String fileName) {
        if (!fileExists()) {
            if (resourceExists(fileName)) {
                ConfigUtils.writeResourceToFile(fileName, new File(filePath));
            } else {
                try {
                    new YamlConfiguration().save(new File(filePath));
                } catch (Exception e) {
                    Main.instance.getLogger().log(Level.WARNING, "Failed to save config file to; " + filePath);
                }

            }
        }
    }

    /**
     * Check to see if an embedded resource exists
     *
     * @param resource The resource to query
     * @return True: Exists, False: Doesn't exist
     */
    public boolean resourceExists(String resource) {
        return (Main.instance.getResource(resource) != null);
    }

    /**
     * Save the configuration file
     *
     * @param config File to be saved
     * @param filePath The path of the file to save
     */
    public static boolean save(YamlConfiguration config, String filePath) {
        try {
            config.save(new File(filePath));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}

