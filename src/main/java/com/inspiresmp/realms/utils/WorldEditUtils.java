package com.inspiresmp.realms.utils;

import com.inspiresmp.realms.Main;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class WorldEditUtils {

    public boolean saveSchematic(String name, World world, Location loc1, Location loc2) {
        WorldEditPlugin wep = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
        if (wep == null) return false;

        TerrainManager tm = new TerrainManager(wep, world);

        Path path = Paths.get(Main.instance.getDataFolder() + File.separator + "schematics" + name + ".schematic");

        File saveFile = new File(Main.instance.getDataFolder() + File.separator + "schematics", name);

        if (saveFile.exists()) {
            return false;
        }

        try {
            Files.createDirectories(path.getParent());
            tm.saveTerrain(saveFile, loc1, loc2);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void loadSchematic(String name, World world, Location loc) {
        WorldEditPlugin wep = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
        if (wep == null) return;

        TerrainManager tm = new TerrainManager(wep, world);
        if (loc != null) {
            try {
                tm.loadSchematic(new File(Main.instance.getDataFolder() + File.separator + "schematics", name), loc);
            } catch (Exception e) {
                Main.instance.getLogger().log(Level.WARNING, "Could not load schematic \"" + name + "\"");
            }
        } else {
            try {
                tm.loadSchematic(new File(Main.instance.getDataFolder() + File.separator + "schematics", name));
            } catch (Exception e) {
                Main.instance.getLogger().log(Level.WARNING, "Could not load schematic \"" + name + "\"");
            }

        }
    }


}

