package com.inspiresmp.realms.utils;

import com.inspiresmp.realms.realms.Realm;
import com.inspiresmp.realms.utils.messages.Message;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class PlayerUtils {

    // Use for authentication purposes
    public static HashMap<UUID, Integer> authentication = new HashMap<>();
    // Invites list
    private static HashMap<UUID, HashMap<String, Realm>> requests = new HashMap<>();

    /**
     * Sends a CommandSender a formatted message
     *
     * @param sender  The CommandSender to send the message to
     * @param message The message to be sent to the player
     */
    public static void sendMessage(CommandSender sender, String message) {
        if (sender instanceof Player) {
            if (!(((Player) sender).isOnline())) {
                return;
            }
        }
        new Message(message).send(sender);
    }

    /**
     * Sends the player a random authentication code
     *
     * @param player The player to generate the authentication code to
     */
    public static void generateAuthenticationCode(Player player) {
        int authCode = new Random().nextInt(8999) + 1000;
        if (authentication.containsKey(player.getUniqueId())) {
            authCode = authentication.get(player.getUniqueId());
        } else {
            authentication.put(player.getUniqueId(), authCode);
        }
        sendMessage(player, "&6Your authentication code is: &3&l" + authCode);
    }

    /**
     * Check to see if the player has a pending request
     *
     * @param player The player to check
     * @return True: Player has pending request, False: Player doesn't have pending request
     */
    public static boolean hasPendingRequest(Player player) {
        return hasPendingRequest(player.getUniqueId());
    }

    /**
     * Check to see if the player has a pending request using their UUID
     *
     * @param player The player to check using their UUID
     * @return True: Player has pending request, False: Player doesn't have pending request
     */
    public static boolean hasPendingRequest(UUID player) {
        return requests.containsKey(player);
    }

    /**
     * Add a request for a player
     *
     * @param firstPlayer The player to add the request
     * @param action      The action to be performed
     * @return True: Added, False: Not added
     */
    public static boolean addRequest(Player firstPlayer, HashMap<String, Realm> action) {
        return addRequest(firstPlayer.getUniqueId(), action);
    }


    /**
     * Add a request for a player using their UUID
     *
     * @param firstPlayer The player to add the request to using their UUID
     * @param action      The action to be performed
     * @return True: Added, False: Not added
     */
    public static boolean addRequest(UUID firstPlayer, HashMap<String, Realm> action) {
        if (hasPendingRequest(firstPlayer)) {
            return false;
        }

        requests.put(firstPlayer, action);

        return true;
    }

    /**
     * Remove a request for a player
     *
     * @param player The player to remove the request for
     * @return True: Remove, False: Not removed
     */
    public static boolean removeRequest(Player player) {
        return removeRequest(player.getUniqueId());
    }

    /**
     * Remove a request for a player using their UUID
     *
     * @param player The player to remove the request for using their UUID
     * @return True: Remove, False: Not removed
     */
    public static boolean removeRequest(UUID player) {
        if (!hasPendingRequest(player)) {
            return false;
        }

        requests.remove(player);

        return true;
    }

    /**
     * Gets a pending request for a player
     *
     * @param player The player to get the request for
     * @return HashMap containing the action
     */
    public static HashMap<String, Realm> getRequest(Player player) {
        return getRequest(player.getUniqueId());
    }

    /**
     * Gets a pending request for a player using their UUID
     *
     * @param player The player to get the request for using their UUID
     * @return HashMap containing the action
     */
    public static HashMap<String, Realm> getRequest(UUID player) {
        if (!hasPendingRequest(player)) {
            return null;
        }
        return requests.get(player);
    }

}
