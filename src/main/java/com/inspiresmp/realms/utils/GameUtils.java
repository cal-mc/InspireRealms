package com.inspiresmp.realms.utils;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.particles.ParticleEffect;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class GameUtils {

    /**
     * Gets the plugin name with formatting
     *
     * @return Plugin name with formatted text
     */
    public static String getPluginName() {
        return ChatColor.translateAlternateColorCodes('&', getRawPluginName());
    }

    /**
     * Gets the plugin name
     *
     * @return Plugin name
     */
    public static String getRawPluginName() {
        return (Main.instance.getConfig().getString("name") != null) ? Main.instance.getConfig().getString("name") : Main.instance.getName();
    }

    /**
     * Gets the plugin display name with formatting
     *
     * @return Plugin display name with formatted text
     */
    public static String getPluginDisplayName() {
        return ChatColor.translateAlternateColorCodes('&', getRawPluginDisplayName());
    }

    /**
     * Gets the plugin display name
     *
     * @return Plugin display name
     */
    public static String getRawPluginDisplayName() {
        return (Main.instance.getConfig().getString("displayName") != null) ? Main.instance.getConfig().getString("displayName") : getRawPluginName();
    }

    /**
     * Gets the default realm size used during realm creation
     *
     * @return The default realm size
     */
    public static int getDefaultRealmSize() {
        return (Main.instance.getConfig().getBoolean("defaultRealmSize")) ? Main.instance.getConfig().getInt("defaultRealmSize") : 50;
    }

    /**
     * Gets the spawn world
     *
     * @return Spawn world
     */
    public static World getSpawnWorld() {
        World spawnWorld = null;

        if (Main.instance.getConfig().get("spawnWorld") != null) {
            spawnWorld = Bukkit.getWorld(Main.instance.getConfig().getString("spawnWorld"));
        }

        return spawnWorld;
    }

    /**
     * Convert a location into a string
     *
     * @param location Location to be turned into a string
     * @return Location in a string
     */
    public static String locationToString(Location location) {
        StringBuilder locationString = new StringBuilder();

        locationString.append(location.getWorld().getName()).append(",")
                .append(location.getX()).append(",")
                .append(location.getY()).append(",")
                .append(location.getZ()).append(",")
                .append(location.getYaw()).append(",")
                .append(location.getPitch());

        return locationString.toString();
    }

    /**
     * Convert a string into a location
     *
     * @param toLocation The string to be converted into a location
     * @return Location from a string
     */
    public static Location stringToLocation(String toLocation) {
        String[] locationString = toLocation.split(",");
        Location location = new Location(Main.instance.getServer().getWorld(locationString[0]), 0, 0, 0);
        location.setX(Double.parseDouble(locationString[1]));
        location.setY(Double.parseDouble(locationString[2]));
        location.setZ(Double.parseDouble(locationString[3]));
        location.setYaw(Float.parseFloat(locationString[4]));
        location.setPitch(Float.parseFloat(locationString[5]));

        return location;
    }

    public static int index = 1;

    public static void makeSpiral(Location location, float particles, float radius, int strands, int curve) {
        ParticleEffect flame = ParticleEffect.FLAME;
        ParticleEffect ench = ParticleEffect.ENCHANTMENT_TABLE;
        ParticleEffect witch = ParticleEffect.SPELL_WITCH;

        int visibleRange = 24;
        for (int i = 1; i <= strands; i++) {
            for (int j = 1; j <= particles; j++) {
                double rotation = (Math.PI / 2 ) + index;
                float ratio = j / particles;
                double angle = curve * ratio * 2 * Math.PI / strands + (2 * Math.PI * i / strands) + rotation;
                double x = Math.cos(angle) * ratio * radius;
                double y = Math.sin(angle) * ratio * radius;
                location.add(x, y, 0);
                flame.display(0, 0, 0, 0.001F, 1, location, visibleRange);
                ench.display(0, 0, 0, 0.000000000000000000000000000000000000000000001F, 1, location, visibleRange);
                location.subtract(x, y, 0);
                index = index + 10;
            }
            witch.display(0, 0, 0, 0.1F, 1, location, visibleRange);
        }
    }

}
