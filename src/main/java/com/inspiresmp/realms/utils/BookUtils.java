package com.inspiresmp.realms.utils;

import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class BookUtils {

    /**
     * Used for authentication *
     */
    public static HashMap<UUID, String> delete = new HashMap<>();

    /**
     * Adds a book to a specific config file
     *
     * @param player The player that is trying to add the book to the config
     * @param config The config file to add the book to
     * @param filePath The filepath in which the config is stored on the disk
     * @param configPath The path in which to save the book in the config file
     */
    public static void addBookToConfig(Player player, YamlConfiguration config, String filePath, String configPath) {
        if (config.get(configPath) != null) {
            PlayerUtils.sendMessage(player, "&cBook already exists!\n &bIf you would like to delete this book Type &6&l\"yes\" &b to delete it!");
            if (delete.containsKey(player.getUniqueId())) return;
            delete.put(player.getUniqueId(), configPath);
            return;
        }

        ItemStack playerItem = player.getItemInHand();

        if (!(playerItem.getItemMeta() instanceof BookMeta)) {
            PlayerUtils.sendMessage(player, "&cYou must be holding a book!");
            return;
        }

        BookMeta bookMeta = (BookMeta) playerItem.getItemMeta();

        config.set(configPath + ".author", bookMeta.getAuthor());
        config.set(configPath + ".displayName", bookMeta.getDisplayName());
        config.set(configPath + ".title", bookMeta.getTitle());
        config.set(configPath + ".lore", bookMeta.getLore());
        config.set(configPath + ".pages", bookMeta.getPages());

        ConfigUtils.save(config, filePath);

        PlayerUtils.sendMessage(player, "&aBook added to config!");
    }

    public static ItemStack getBookFromConfig(YamlConfiguration config, String configPath) {
        if (config.getString(configPath) == null) {
            return null;
        }

        ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta bookMeta = (BookMeta) book.getItemMeta();

        List<String> pages = config.getStringList(configPath + ".pages");

        bookMeta.setAuthor(config.getString(configPath + ".author"));
        bookMeta.setDisplayName(config.getString(configPath + ".displayName"));
        bookMeta.setTitle(config.getString(configPath + ".title"));
        bookMeta.setLore(config.getStringList(configPath + ".lore"));
        bookMeta.setPages(config.getStringList(configPath + ".pages"));

        book.setItemMeta(bookMeta);

        return book;
    }

    public static boolean deleteBookFromConfig(YamlConfiguration config, String filePath, String configPath) {
        if (config.get(configPath) != null) {
            config.set(configPath, null);
            ConfigUtils.save(config, filePath);
            return true;
        }
        return false;
    }

}

