package com.inspiresmp.realms.tokens;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.config.PlayerConfig;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class TokenManager {

    /**
     * Sets the amount of tokens a player has
     *
     * @param player UUID of the player
     * @param newBalance The amount of tokens the player will have
     * @param safe Check to see if the player can purchase the item first
     */
    public static void setTokens(UUID player, int newBalance, boolean safe) {
        if (safe && !canPurchase(player, newBalance)) {
            return;
        }
        YamlConfiguration config = PlayerConfig.getFile(player);
        config.set("balance", newBalance);
        PlayerConfig.save(config,player);
    }

    /**
     * Gets the tokens for a player
     *
     * @param player UUID of the player
     * @return Amount of tokens
     */
    public static int getTokens(UUID player) {
        try {
            return Integer.parseInt(PlayerConfig.getFile(player).getString("balance"));
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Can the player afford to purchase that item
     *
     * @param player UUID of the player
     * @param productAmount The amount of the item to purchase
     * @return True: Can purchase, False: Can't purchase
     */
    public static boolean canPurchase(UUID player, int productAmount) {
        return (productAmount > getTokens(player));
    }

    /**
     * Gets the amount of time between giving tokens to player while they are logged in
     * @return The time of the timer
     */
    public static int getTimerTime(){
        if(Main.instance.getConfig().get("tokens.time") == null) {
            return 0;
        }
        return Main.instance.getConfig().getInt("tokens.time");
    }

    /**
     * Gets the amount of tokens to give the player every getTimerTime() seconds
     * @return The amount of tokens to give the player
     */
    public static int getTimerAmount(){
        if(Main.instance.getConfig().get("tokens.amount") == null) {
            return 0;
        }
        return Main.instance.getConfig().getInt("tokens.amount");
    }

    /**
     * Starts the process in which to give the player getTimerAmount() tokens every getTimerTime()
     * @param player UUID of the player to start the timer for
     */
    public static void startTokenTimer(final UUID player){
        new BukkitRunnable(){
            @Override
            public void run() {
                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player);
                if(!offlinePlayer.isOnline()){
                    this.cancel();
                    return;
                }
                setTokens(player, getTokens(player) + getTimerAmount(), false);
            }
        }.runTaskTimer(Main.instance, getTimerTime(), getTimerTime());
    }
}
