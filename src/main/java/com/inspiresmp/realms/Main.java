package com.inspiresmp.realms;

import com.inspiresmp.realms.blocks.BlockManager;
import com.inspiresmp.realms.commands.SpawnCommand;
import com.inspiresmp.realms.commands.TokenCommand;
import com.inspiresmp.realms.commands.TutorialCommand;
import com.inspiresmp.realms.config.*;
import com.inspiresmp.realms.listeners.*;
import com.inspiresmp.realms.menu.InventoryMenuEventListener;
import com.inspiresmp.realms.realms.Realm;
import com.inspiresmp.realms.commands.RealmCommand;
import com.inspiresmp.realms.realms.RealmManager;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class Main extends JavaPlugin {

    public static Main instance;
    public Random random;

    /**
     * Called when the plugin enables
     */
    @Override
    public void onEnable() {
        init();
    }

    /**
     * Called when the plugin disables
     */
    @Override
    public void onDisable() {
        getLogger().log(Level.INFO, "Resetting blocks..");
        BlockManager.flushBlocks();
        getLogger().log(Level.INFO, "Blocks reset");
        getLogger().log(Level.INFO, "Unloading realms...");
        for (Realm realm : new ArrayList<>(RealmManager.getManager().getRealms())) {
            RealmManager.getManager().unloadRealm(realm);
        }
        getLogger().log(Level.INFO, "Realms unloaded!");
    }

    /**
     *  The method that is used to initialize things
     */
    private void init(){
        instance = this;
        random = new Random();

        /** Register things **/
        registerCommands();
        registerListeners();

        /** Generate configs **/
        this.getConfig().options().copyDefaults(true);
        saveDefaultConfig();

        /** Load custom configs **/
        RealmConfig.getFile();
        EntityConfig.getFile();
        ItemConfig.getFile();
        TokenConfig.getFile();
        BlockConfig.getFile();
        AltarConfig.getFile();
        AltarConfig.generateDefaultSchematics();

        /** Setup things **/
        setupWorldborder();
    }

    /**
     * Register commands for the plugin
     */
    public void registerCommands() {
        getCommand("realm").setExecutor(new RealmCommand());
        getCommand("spawn").setExecutor(new SpawnCommand());
        getCommand("token").setExecutor(new TokenCommand());
        getCommand("redeem").setExecutor(new TokenCommand());
        getCommand("tutorial").setExecutor(new TutorialCommand());
    }

    /**
     * Register events for the plugin
     */
    public void registerListeners() {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new BlockListener(), this);
        pluginManager.registerEvents(new ChatListener(), this);
        pluginManager.registerEvents(new PlayerListener(), this);
        pluginManager.registerEvents(new RealmListener(), this);
        pluginManager.registerEvents(new InventoryMenuEventListener(), this);
        pluginManager.registerEvents(new EntityListener(), this);
    }

    /**
     * Used to setup the worldborder
     */
    public void setupWorldborder() {
        YamlConfiguration config = AltarConfig.getFile();
        int WBX = 0;
        int WBZ = 0;
        if (config.get("worldborder.center.x") != null) {
            WBX = config.getInt("worldborder.center.x");
        }
        if (config.get("worldborder.center.z") != null) {
            WBZ = config.getInt("worldborder.center.z");
        }

        Main.instance.getServer().dispatchCommand(Bukkit.getConsoleSender(), "worldborder center " + WBX + " " + WBZ);
    }

}
