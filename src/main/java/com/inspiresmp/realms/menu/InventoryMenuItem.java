package com.inspiresmp.realms.menu;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Represents an item in an inventory menu.
 * Displays like an ItemStack in an inventory, and activates the onClick method when it's getting selected.
 * It defines what to do when a "button" in a menu is being clicked by a player.
 * Useful for menus which must have an action when clicking a button.
 *
 * @author spaceemotion
 * @author CaLxCyMru
 * @version 1.0
 */
public abstract class InventoryMenuItem {
    private InventoryMenu menu;

    private int index;

    private ItemStack itemStack;


    /**
     * Creates a new inventory menu item.
     *
     * @param item The item for the menu item
     */
    public InventoryMenuItem(ItemStack item) {
        this.itemStack = item;
    }

    /**
     * Creates a new inventory menu item with a default material (paper).
     *
     * @param name The item display name
     */
    public InventoryMenuItem(String name) {
        this(name, Material.PAPER);
    }

    /**
     * Creates a new inventory menu item with the given icon material and the given display name.
     *
     * @param name The item display name
     * @param icon The icon material
     */
    public InventoryMenuItem(String name, Material icon) {
        this(name, icon, 1);
    }

    /**
     * Creates a new inventory menu item with the given icon material, display name and amount.
     *
     * @param name   The item display name
     * @param icon   The icon material
     * @param amount The amount of the item stack
     */
    public InventoryMenuItem(String name, Material icon, int amount) {
        itemStack = new ItemFactory(itemStack).setName(name).setType(icon).setAmount(amount).getItem();
    }

    /**
     * Called when a player clicks on a button in a menu
     *
     * @param player       The player clicking the button
     * @param isRightClick True if the right mouse button has been pressed
     * @param isShiftClick True if the shift key has been pressed
     */
    protected abstract void onClick(Player player, boolean isRightClick, boolean isShiftClick);

//    /**
//     * Attempts to move the item to a different index.
//     *
//     * @param i The new index
//     * @return True if successful, false if not
//     */
//    public boolean setIndex(int i) {
//        if (getMenu() == null) {
//            return false;
//        }
//
//        Inventory inventory = getMenu().getInventory();
//
//        // Check inventory size
//        if (i > (inventory.getSize() - 1)) {
//            return false;
//        }
//
//        // We can't really move a different item
//        if (inventory.getItem(i) != null) {
//            return false;
//        }
//
//        getMenu().removeItem(getIndex());
//        getMenu().setItem0(this, i);
//
//        return true;
//    }

    /**
     * Gets the menu
     *
     * @return InventoryMenu
     */
    public InventoryMenu getMenu() {
        return this.menu;
    }

    /**
     * Sets the menu as a another menu
     *
     * @param menu New menu
     * @return Instance of ItemMenuItem class
     */
    public InventoryMenuItem setMenu(InventoryMenu menu) {
        this.menu = menu;
        return this;
    }

    /**
     * Gets the index
     *
     * @return Index of the menu
     */
    public int getIndex() {
        return this.index;
    }

    /**
     * Sets the menu index
     *
     * @param index New index
     * @return Instance of ItemMenuItem class
     */
    public InventoryMenuItem setIndex(int index) {
        this.index = index;
        return this;
    }

    /**
     * Gets the menu
     *
     * @return InventoryMenu
     */
    public ItemStack getItemStack() {
        return this.itemStack;
    }

    /**
     * Sets the item stack for this given menu item.
     *
     * @param item The item for the menu item
     */
    public void setItemStack(ItemStack item) {
        itemStack = item;

        if (getMenu() != null) {
            getMenu().getInventory().setItem(getIndex(), getItemStack());
        }
    }

}
