package com.inspiresmp.realms.menu;

import org.bukkit.entity.Player;

/**
 * Represents a menu close behaviour.
 * Defines what to do when a menu is closed by a player. Useful for menus which must have an action before closing
 *
 * @author spaceemotion
 * @version 1.0
 */
public interface InventoryMenuCloseBehaviour {

    /**
     * Called when a player closes a menu.
     *
     * @param player The player closing the menu
     */
    void onClose(Player player);

}
