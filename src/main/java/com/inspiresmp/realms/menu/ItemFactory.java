package com.inspiresmp.realms.menu;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.config.ItemConfig;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * Crafted in the heart of Wales by CaLxCyMru!
 */
public class ItemFactory {

    // Instance of the ItemFactory
    private ItemFactory instance;

    // ItemStack that will contain the item we create
    private ItemStack item;

    /**
     * Instance of the ItemFactory class
     *
     * @param material The material of the item
     */
    public ItemFactory(Material material) {
        this.instance = this;
        this.item = createItemStack(material, 1, 0, null, null, null);
    }

    /**
     * Instance of the ItemFactory class
     *
     * @param material The material of the item
     * @param amount   The amount of the item
     */
    public ItemFactory(Material material, int amount) {
        this.instance = this;
        this.item = createItemStack(material, amount, 0, null, null, null);
    }

    /**
     * Instance of the ItemFactory class
     *
     * @param material The material of the item
     * @param amount   The amount of the item
     * @param data     The data tag for the item
     */
    public ItemFactory(Material material, int amount, int data) {
        this.instance = this;
        this.item = createItemStack(material, amount, data, null, null, null);
    }

    /**
     * Instance of the ItemFactory class using a pre existing ItemStack
     *
     * @param item The pre existing ItemStack
     */
    public ItemFactory(ItemStack item) {
        this.instance = this;
        this.item = item;
    }

    /**
     * Gets the type of the Item
     *
     * @return Item Material
     */
    public Material getType() {
        return item.getType();
    }

    /**
     * Sets the type of the Item
     *
     * @param newType The new material
     * @return Instance of ItemFactory class
     */
    public ItemFactory setType(Material newType) {
        item.setType(newType);
        return this.instance;
    }

    /**
     * Gets the amount of the item
     *
     * @return The amount
     */
    public int getAmount() {
        return item.getAmount();
    }

    /**
     * Sets the amount of the item
     *
     * @param amount New amount for the item
     * @return Instance of ItemFactory class
     */
    public ItemFactory setAmount(int amount) {
        item.setAmount(amount);
        return this.instance;
    }

    /**
     * Gets the data of the item
     *
     * @return The data of the item
     */
    public int getData() {
        return item.getDurability();
    }

    /**
     * Sets the data of an item
     *
     * @param data The data for the item
     * @return Instance of the ItemFactory class
     */
    public ItemFactory setData(int data) {
        item.setDurability((short) data);
        return this.instance;
    }

    /**
     * Gets the skull owners name
     *
     * @return The name of the skull owner
     */
    public String getSkullOwner() {
        if (getType() == Material.SKULL_ITEM && getData() == 3) {
            return ((SkullMeta) getMeta()).getOwner();
        }
        return null;
    }

    /**
     * Sets the skull owner of the item
     *
     * @param owner The owner
     * @return Instance of the ItemFactory class
     */
    public ItemFactory setSkullOwner(String owner) {
        if (getType() == Material.SKULL_ITEM && getData() == 3) {
            SkullMeta skullMeta = (SkullMeta) getMeta();
            skullMeta.setOwner(owner);
            setMeta(skullMeta);
        }
        return this.instance;
    }

    /**
     * Formats a string with minecraft colour codes
     *
     * @param toFormat The string to be formatted
     * @return The formatted string
     */
    public String addFormatting(String toFormat) {
        if (!toFormat.contains("&")) return toFormat;
        return toFormat.replace("&", ChatColor.COLOR_CHAR + "");
    }

    /**
     * Gets the ItemStack from the factory
     *
     * @return The ItemStack that has been used within the factory
     */
    public ItemStack getItem() {
        return item;
    }

    /**
     * Gets the ItemMeta
     *
     * @return The ItemMeta of the item being used in the factory
     */
    public ItemMeta getMeta() {
        return item.getItemMeta();
    }

    /**
     * Sets the ItemMeta of the item
     *
     * @param meta New ItemMeta to be applied
     * @return Instance of the ItemFactory class
     */
    public ItemFactory setMeta(ItemMeta meta) {
        this.item.setItemMeta(meta);
        return this.instance;
    }

    /**
     * Gets the name of the item in the factory
     *
     * @return The name of the ItemStack using its ItemMeta
     */
    public String getName() {
        ItemMeta meta = getMeta();
        if (meta.getDisplayName() != null) {
            return meta.getDisplayName();
        }
        return null;
    }

    /**
     * Sets the name of the item and handles formatting
     *
     * @param name New name for the item
     * @return Instance of the ItemFactory class
     */
    public ItemFactory setName(String name) {
        ItemMeta meta = getMeta();
        meta.setDisplayName(addFormatting(name));
        setMeta(meta);
        return this.instance;
    }

    /**
     * Gets the lore of the item in the factory
     *
     * @return The lore of the ItemStack using its ItemMeta
     */
    public List<String> getLore() {
        ItemMeta meta = getMeta();
        if (meta.getLore() != null) {
            return meta.getLore();
        }
        return null;
    }

    /**
     * Sets the lore of the item and handles formatting
     *
     * @param lore New lore for the item
     * @return Instance of the ItemFactory class
     */
    public ItemFactory setLore(String lore) {
        List<String> newLore = new ArrayList<>();
        if (lore.contains("\n")) {
            String[] lines = lore.split("\n");
            for (String line : lines) {
                newLore.add(addFormatting(line));
            }
        } else {
            newLore.add(addFormatting(lore));
        }

        ItemMeta meta = getMeta();
        meta.setLore(newLore);
        setMeta(meta);

        return this.instance;
    }

    /**
     * Gets the enchantments applied to the item in the factory
     *
     * @return The enchantments that are applied to the ItemStack using its ItemMeta
     */
    public Map<Enchantment, Integer> getEnchantments() {
        ItemMeta meta = getMeta();
        if (meta.getEnchants() != null) {
            return meta.getEnchants();
        }
        return null;
    }

    /**
     * Sets the enchantments for the item
     *
     * @param enchantments Enchantments to be applied to the item
     * @return Instance of the ItemFactory class
     */
    public ItemFactory setEnchantments(List<String> enchantments) {
        for (String enchantment : enchantments) {
            if (!enchantment.contains(":")) continue;

            String[] enchantList = enchantment.split(":");

            String enchantmentType = enchantList[0];
            int enchantmentLevel = Integer.parseInt(enchantList[1]);

            try {
                getItem().addUnsafeEnchantment(Enchantment.getByName(enchantmentType), enchantmentLevel);
            } catch (Exception e) {
                Main.instance.getLogger().log(Level.WARNING, "Unable to add enchantment [" + enchantmentType + "] of level [" + enchantmentLevel + "] to a ItemStack!");
            }
        }
        return this.instance;
    }

    /**
     * Create an ItemStack
     *
     * @param material     The material of the item
     * @param amount       The amount of the item
     * @param data         The data tag for the item
     * @param name         The name of the item
     * @param lore         The lore of the item
     * @param enchantments The enchantments of the item
     * @return The new ItemStack that has been created using all the parameters
     */
    public ItemStack createItemStack(Material material, int amount, int data, String name, List<String> enchantments, String lore) {
        ItemStack item = new ItemStack(material, amount, (short) data);
        ItemMeta meta = item.getItemMeta();
        if (name != null) {
            setName(name);
        }
        if (lore != null) {
            setLore(lore);
        }
        if (enchantments != null) {
            setEnchantments(enchantments);
        }
        item.setItemMeta(meta);
        return item;
    }

    public static String getItemName(ItemStack item){
        String name;
        YamlConfiguration config = ItemConfig.getFile();
        if(config.getString(item.getType().toString()) != null){
            name = config.getString(item.getType().toString());
        } else {
            name = item.getType().toString().toLowerCase().replaceAll("_", " ");
        }

        return name;
    }

}
