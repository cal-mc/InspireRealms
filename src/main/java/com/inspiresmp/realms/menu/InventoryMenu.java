package com.inspiresmp.realms.menu;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.LinkedList;

/**
 * Represents a custom inventory that functions as a menu.
 *
 * @author spaceemotion
 * @version 1.0
 */
public class InventoryMenu implements InventoryHolder {
    /**
     * The default number if slots per row
     */
    public final static int ROW_SIZE = 9;

    private InventoryMenuItem[] items;
    private Inventory inventory;
    private String title;
    private int size;
    private InventoryMenuCloseBehaviour closeBehaviour;

    /**
     * Creates a new inventory menu.
     *
     * @param title The menu title
     * @param rows  The number of rows
     */
    public InventoryMenu(String title, int rows) {
        this.title = title;
        this.size = rows * ROW_SIZE;

        this.items = new InventoryMenuItem[size];
    }

    /**
     * Calculates the number of required rows for the given slot size.
     *
     * @param slots The number of slots
     * @return The number of rows needed
     */
    public static int calculateRows(double slots) {
        return (int) Math.ceil(slots / (double) ROW_SIZE);
    }

    /**
     * Gets the currently assigned menu close behaviour.
     *
     * @return The menu close behaviour, or null if nothing has been set.
     */
    @Nullable
    public InventoryMenuCloseBehaviour getMenuCloseBehaviour() {
        return closeBehaviour;
    }


	/* -------- Inventory related functions -------- */

    /**
     * Sets the menu close behaviour.
     * Set this to null if nothing should happen when the menu gets closed.
     *
     * @param menuCloseBehaviour The menu close behaviour
     */
    public void setMenuCloseBehaviour(@Nullable InventoryMenuCloseBehaviour menuCloseBehaviour) {
        this.closeBehaviour = menuCloseBehaviour;
    }

    @Override
    public Inventory getInventory() {
        if (inventory == null) {
            inventory = Bukkit.createInventory(this, size, title);
        }

        return inventory;
    }

    /**
     * Adds an item to the menu.
     *
     * @param item The item to add
     * @param x    The x-position on the menu
     * @param y    The y-position on the menu
     * @return True if we were able to add the item, false if not
     */
    public boolean setItem(InventoryMenuItem item, int x, int y) {
        return setItem(item, y * ROW_SIZE + x);
    }

    /**
     * Adds an item to the specified inventory index.
     *
     * @param item  The item to add
     * @param index The index of the item
     * @return True if we were able to add the item, false if not
     */
    public boolean setItem(InventoryMenuItem item, int index) {
        if (outOfBounds(index)) {
            return false;
        }

        ItemStack slot = getInventory().getItem(index);

        if (slot != null && slot.getType() != Material.AIR) {
            return false;
        }

        items[index] = item;
        item.setMenu(this);
        item.setIndex(index);

        getInventory().setItem(index, item.getItemStack());

        return true;
    }

    /**
     * Removes the item on the specified slot.
     *
     * @param index The index of the item
     */
    public void removeItem(int index) {
        if (outOfBounds(index)) {
            return;
        }

        items[index] = null;
        getInventory().setItem(index, null);
    }

    /**
     * Opens the menu for the given player.
     *
     * @param player The player to open the menu for
     */
    public void openMenu(HumanEntity player) {
        if (player == null) {
            return;
        }

        if (getInventory().getViewers().contains(player)) {
            throw new IllegalArgumentException(player.getName() + " is already viewing " + getInventory().getTitle());
        }
        player.closeInventory();
        player.openInventory(getInventory());
    }

    /**
     * Updates the menu for all viewers.
     */
    public void updateMenu() {
        for (HumanEntity entity : getInventory().getViewers()) {
            ((Player) entity).updateInventory();
        }
    }

    /**
     * Closes the menu for all current viewers.
     */
    public void closeMenu() {
        for (HumanEntity viewer : new LinkedList<>(getInventory().getViewers())) {
            closeMenu(viewer);
        }
    }

    /**
     * Closes the menu for the given viewer.
     *
     * @param viewer The viewer to close the menu for
     */
    public void closeMenu(HumanEntity viewer) {
        if (!getInventory().getViewers().contains(viewer)) {
            return;
        }

        InventoryCloseEvent event = new InventoryCloseEvent(viewer.getOpenInventory());
        Bukkit.getPluginManager().callEvent(event);
        viewer.closeInventory();
    }

    // -------- General override functions --------

    @Override
    public String toString() {
        return "InventoryMenu{title=" + title + ", size=" + size + "}";
    }

    // -------- Private / protected helper functions --------

    private boolean outOfBounds(int index) {
        return index < 0 || index > size;
    }

    void setItem0(InventoryMenuItem item, int index) {
        items[index] = item;
        getInventory().setItem(index, item.getItemStack());
    }

    void selectItem(Player player, int index) {
        selectItem(player, index, false, false);
    }

    void selectItem(Player player, int index, boolean right, boolean shift) {
        InventoryMenuItem item = items[index];

        if (item != null) {
            item.onClick(player, right, shift);
        }
    }

}
