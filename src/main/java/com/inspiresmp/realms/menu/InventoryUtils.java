package com.inspiresmp.realms.menu;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class InventoryUtils {

    /**
     * Gets the nearest row to 9
     *
     * @param toCheck The number to check
     * @return The nearest 9
     */
    public static int getClosestNine(int toCheck) {
        int base = 9;
        if (toCheck == 0) {
            return 0;
        } else if (toCheck > 0 && toCheck <= (base)) {
            return 1;
        } else if (toCheck > (base) && toCheck <= (base * 2)) {
            return 2;
        } else if (toCheck > (base * 2) && toCheck <= (base * 3)) {
            return 3;
        } else if (toCheck > (base * 3) && toCheck <= (base * 4)) {
            return 4;
        } else if (toCheck > (base * 4) && toCheck <= (base * 5)) {
            return 5;
        } else if (toCheck > (base * 5) && toCheck <= (base * 6)) {
            return 6;
        } else if (toCheck > (base * 6) && toCheck <= (base * 7)) {
            return 7;
        } else if (toCheck > (base * 7) && toCheck <= (base * 8)) {
            return 8;
        } else if (toCheck > (base * 8) && toCheck <= (base * 9)) {
            return 9;
        } else {
            return 0;
        }
    }


}
