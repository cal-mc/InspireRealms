package com.inspiresmp.realms.commands;

import com.inspiresmp.realms.realms.Realm;
import com.inspiresmp.realms.realms.RealmGUI;
import com.inspiresmp.realms.realms.RealmManager;
import com.inspiresmp.realms.utils.PlayerUtils;
import com.inspiresmp.realms.utils.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class RealmCommand implements CommandExecutor {

    // The player that is sending the command
    private Player player;
    // The player's realm
    private Realm realm;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] subcommands) {
        if (command.getName().equalsIgnoreCase("realm")) {
            if (!(sender instanceof Player)) {
                PlayerUtils.sendMessage(sender, "&cOnly players can use this command!");
                return true;
            }

            this.player = (Player) sender;
            this.realm = new Realm(this.player);

            // Display the help message if no arguments are entered
            if (subcommands.length == 0) {
                if (!this.realm.hasRealm()) {
                    this.realm.createRealm();
                }
                RealmManager.getManager().teleport(this.player, this.realm);
                return true;

            } else if (subcommands.length >= 1) {
                if (subcommands.length == 1) {
                    switch (subcommands[0].toLowerCase()) {
                        case "test":
                            for (ItemStack item : realm.getRequiredResources()) {
                                player.getInventory().addItem(item);
                            }
                            return true;
                        case "accept":
                            if (!PlayerUtils.hasPendingRequest(this.player)) {
                                PlayerUtils.sendMessage(this.player, "&cYou do not have any pending requests!");
                                return true;
                            }
                            performAction(PlayerUtils.getRequest(this.player).keySet().iterator().next());
                            PlayerUtils.removeRequest(player);
                            return true;
                        case "decline":
                            if (!PlayerUtils.hasPendingRequest(this.player)) {
                                PlayerUtils.sendMessage(this.player, "&cYou do not have any pending requests!");
                                return true;
                            }
                            Realm realm = PlayerUtils.getRequest(this.player).get(PlayerUtils.getRequest(this.player).keySet().iterator().next());

                            OfflinePlayer realmOwner = Bukkit.getOfflinePlayer(realm.getOwner());
                            PlayerUtils.removeRequest(this.player);
                            PlayerUtils.sendMessage(this.player, "&bYou have declined &7&l" + realmOwner.getName() + "'s &binvite!");
                            PlayerUtils.sendMessage(realmOwner.getPlayer(), "&7&l" + this.player.getName() + " &ahas declined your invite!");
                            return true;
                        case "stats":
                            if (!this.realm.hasRealm()) {
                                PlayerUtils.sendMessage(this.player, "&cYou do not have a realm!");
                                return true;
                            }
                            RealmManager.getManager().displayStats(this.realm, this.player);
                            PlayerUtils.sendMessage(this.player, "&aNow displaying your statistics!");
                            return true;
                        case "purge":
                            if (!this.realm.hasRealm()) {
                                PlayerUtils.sendMessage(this.player, "&cYou do not have a realm!");
                                return true;
                            }
                            PlayerUtils.sendMessage(this.player, "&4*&lWARNING&4* This will WIPE your realm!\n&cTo purge your real, please type your authentication code in chat");
                            PlayerUtils.generateAuthenticationCode(this.player);
                            return true;
                        case "list":
                            if (!this.realm.hasRealm()) {
                                PlayerUtils.sendMessage(this.player, "&cYou do not have a realm!");
                                return true;
                            }
                            RealmGUI.openListGUI(player);
                            return true;
                        case "players":
                            if (!this.realm.hasRealm()) {
                                PlayerUtils.sendMessage(this.player, "&cYou do not have a realm!");
                                return true;
                            }
                            RealmGUI.openPlayerGUI(player);
                            return true;
                        case "upgrade":
                            if (!this.realm.hasRealm()) {
                                PlayerUtils.sendMessage(this.player, "&cYou do not have a realm!");
                                return true;
                            }
                            RealmGUI.openUpgradeGUI(player);
                            return true;
                        default:
                            sendHelpMessage(commandLabel);
                            return true;
                    }
                } else if (subcommands.length == 2) {
                    OfflinePlayer targetPlayer;
                    try {
                        targetPlayer = Bukkit.getOfflinePlayer(UUIDFetcher.getUUIDOf(subcommands[1]));
                    } catch (Exception e) {
                        PlayerUtils.sendMessage(player, "&cFailed to get UUID of &7&l" + subcommands[1]);
                        return true;
                    }
                    Realm targetRealm = new Realm(targetPlayer);
                    if (subcommands[0].equalsIgnoreCase("warp")) {
                        if (player.hasPermission("realm.warp")) {
                            if (!targetRealm.hasRealm()) {
                                PlayerUtils.sendMessage(player, "&7&l" + subcommands[1] + " &cdoes not have a realm!");
                                return true;
                            }
                            RealmManager.getManager().teleport(this.player, targetRealm);
                            return true;
                        } else {
                            PlayerUtils.sendMessage(player, "&cYou do not have permission to perform this command!");
                            return true;
                        }
                    }

                    if (!this.realm.hasRealm()) {
                        PlayerUtils.sendMessage(this.player, "&cYou do not have a realm!");
                        return true;
                    }
                    if (subcommands[0].equalsIgnoreCase("kick")) {
                        if (!this.realm.getMembers().contains(UUIDFetcher.getUUID(subcommands[1]))) {
                            PlayerUtils.sendMessage(this.player, "&cThat player is not in your realm!");
                            return true;
                        }
                        if (this.realm.kickPlayer(this.player)) {
                            PlayerUtils.sendMessage(this.player, "&7&l" + subcommands[1] + " &ahas been kicked from your realm!");
                        }
                        return true;
                    }
                    // Loop through the online players to see if one of their names matches the last command argument
                    for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
                        if (onlinePlayer.getName().equalsIgnoreCase(subcommands[1])) {
                            switch (subcommands[0].toLowerCase()) {
                                case "summon":
                                    addAction(onlinePlayer, subcommands);
                                    return true;
                                case "invite":
                                    addAction(onlinePlayer, subcommands);
                                    return true;
                                default:
                                    sendHelpMessage(commandLabel);
                                    return true;
                            }
                        }
                    }

                    PlayerUtils.sendMessage(player, "&cThe target player could not be found!");
                    return true;

                }
            }

            return true;
        }

        return false;
    }

    /**
     * Send the player the help message.
     *
     * @param commandLabel The command label the player has used.
     */
    public void sendHelpMessage(String commandLabel) {
        StringBuilder help = new StringBuilder();
        String commandPrefix = "\n&c/" + commandLabel + "&a ";

        help.append("&7>> &6Help &7<<")
                .append(commandPrefix).append("\n   &6Teleports the player to their realm")
                .append(commandPrefix).append("help\n    &6Displays this help file")
                .append(commandPrefix).append("accept\n   &6Accepts a pending request")
                .append(commandPrefix).append("decline\n   &6Declines a pending request")
                .append(commandPrefix).append("list\n    &6Displays a GUI that shows which realms are accessible to the player")
                .append(commandPrefix).append("players\n    &6Displays a GUI that show which players are accessible to your realm")
                .append(commandPrefix).append("purge\n    &6Deletes your realm")
                .append(commandPrefix).append("stats\n    &6Displays statistics for the current realm")
                .append(commandPrefix).append("upgrade\n   &6Displays the realm upgrade GUI")
                .append(commandPrefix).append("kick <Player>\n    &6Kicks a player from your realm")
                .append(commandPrefix).append("invite <Player>\n    &6Invites a player to your realm")
                .append(commandPrefix).append("summon <Player>\n    &6Summons a player to your realm");

        PlayerUtils.sendMessage(this.player, help.toString());
    }

    /**
     * Adds a request for a player with a specific action
     *
     * @param player      The player to add the request for
     * @param subcommands The command arguments
     * @return True: Added, False: Not added
     */
    private boolean addAction(Player player, String[] subcommands) {
        HashMap<String, Realm> action = new HashMap<>();
        if (subcommands[1].equalsIgnoreCase(this.player.getName())) {
            PlayerUtils.sendMessage(this.player, "&cYou cannot invite yourself!");
            return false;
        }
        if (PlayerUtils.hasPendingRequest(player)) {
            PlayerUtils.sendMessage(this.player, "&7&l" + player.getName() + " &chas a pending request!");
            return false;
        }
        action.put("invite", this.realm);

        PlayerUtils.addRequest(player, action);
        PlayerUtils.sendMessage(this.player, "&aYou have sent a request to &7&l" + player.getName());
        PlayerUtils.sendMessage(player, "&bYou have been invited to join &7&l" + this.player.getName() + "'s &brealm!\n&aTo accept please do &r&l/realm accept &aotherwise, &r&l/realm decline");
        return true;
    }

    /**
     * Performs a action from a request
     *
     * @param actionString The action to be performed
     * @return True: Performed, False: Not performed
     */
    private boolean performAction(String actionString) {
        boolean result;
        Realm realm = PlayerUtils.getRequest(this.player).get(actionString);
        OfflinePlayer realmOwner = Bukkit.getOfflinePlayer(realm.getOwner());
        if (actionString.equalsIgnoreCase("summon")) {
            result = realm.summonPlayer(this.player);
        } else if (actionString.equalsIgnoreCase("invite")) {
            result = realm.invitePlayer(this.player);
        } else {
            PlayerUtils.sendMessage(this.player, "&cOops, something went wrong!");
            PlayerUtils.sendMessage(realmOwner.getPlayer(), "&cOops, something went wrong!");
            return false;
        }
        if (result) {
            PlayerUtils.sendMessage(this.player, "&bYou have accepted &7&l" + realmOwner.getName() + "'s &binvite!");
            PlayerUtils.sendMessage(realmOwner.getPlayer(), "&7&l" + this.player.getName() + " &ahas accepted your invite!");
        }
        return result;
    }

}
