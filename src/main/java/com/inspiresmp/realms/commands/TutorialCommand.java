package com.inspiresmp.realms.commands;

import com.inspiresmp.realms.config.TutorialConfig;
import com.inspiresmp.realms.utils.BookUtils;
import com.inspiresmp.realms.utils.PlayerUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class TutorialCommand implements CommandExecutor {

    // The player that is sending the command
    private Player player;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] subcommands) {
        if (command.getName().equalsIgnoreCase("tutorial")) {
            if (!(sender instanceof Player)) {
                PlayerUtils.sendMessage(sender, "&cOnly players can use this command!");
                return true;
            }

            this.player = (Player) sender;
            if (subcommands.length == 0) {
                BookUtils.addBookToConfig(player, TutorialConfig.getFile(), TutorialConfig.filePath, "tutorial.0.book");
                return true;

            }

            return true;
        }

        return false;
    }

    /**
     * Send the player the help message.
     *
     * @param commandLabel The command label the player has used.
     */
    public void sendHelpMessage(String commandLabel) {
        StringBuilder help = new StringBuilder();
        String commandPrefix = "\n&c/" + commandLabel + "&a ";

        help.append("&7>> &6Help &7<<")
                .append(commandPrefix).append("\n   &6Starts the tutorial");

        PlayerUtils.sendMessage(this.player, help.toString());
    }


}
