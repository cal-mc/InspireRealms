package com.inspiresmp.realms.commands;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.utils.GameUtils;
import com.inspiresmp.realms.utils.PlayerUtils;
import com.inspiresmp.realms.utils.WorldUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class SpawnCommand implements CommandExecutor {

    // The player that is sending the command
    private Player player;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] subcommands) {
        if (command.getName().equalsIgnoreCase("spawn")) {
            if (!(sender instanceof Player)) {
                PlayerUtils.sendMessage(sender, "&cOnly players can use this command!");
                return true;
            }

            this.player = (Player) sender;
            Main.instance.reloadConfig();
            // Display the help message if no arguments are entered
            if (subcommands.length == 0) {
                this.player.teleport(WorldUtils.getSpawnLocation());
                PlayerUtils.sendMessage(this.player, "&aYou are now at spawn!");
                return true;

            } else if (subcommands.length >= 1) {
                switch (subcommands[0].toLowerCase()) {
                    case "get":
                        if(!player.hasPermission("inspiresurvival.spawn.get")){
                            PlayerUtils.sendMessage(this.player, "&cYou do not have permission to perform this command!");
                            return true;
                        }
                        String spawnLocation[] = WorldUtils.getRawSpawnLocation().split(",");
                        PlayerUtils.sendMessage(this.player,
                                "\n&a&lWorld: &b" + spawnLocation[0] +
                                "\n&a&lX: &b" + spawnLocation[1] +
                                "\n&a&lY: &b" + spawnLocation[2] +
                                "\n&a&lZ: &b" + spawnLocation[3] +
                                "\n&a&lYaw: &b" + spawnLocation[4] +
                                "\n&a&lPitch &b" + spawnLocation[5]);
                        return true;
                    case "set":
                        if(!player.hasPermission("inspiresurvival.spawn.set")){
                            PlayerUtils.sendMessage(this.player, "&cYou do not have permission to perform this command!");
                            return true;
                        }
                        Main.instance.getConfig().set("spawnLocation", GameUtils.locationToString(this.player.getLocation()));
                        Main.instance.saveConfig();
                        PlayerUtils.sendMessage(this.player, "&aSpawn Location set to where you are standing!");
                        return true;
                }
                sendHelpMessage(commandLabel);
                return true;
            }

            return true;
        }

        return false;
    }

    /**
     * Send the player the help message.
     *
     * @param commandLabel The command label the player has used.
     */
    public void sendHelpMessage(String commandLabel) {
        StringBuilder help = new StringBuilder();
        String commandPrefix = "\n&c/" + commandLabel + "&a ";

        help.append("&7>> &6Help &7<<")
                .append(commandPrefix).append("\n   &6Teleports the player to spawn")
                .append(commandPrefix).append("get\n    &6Gets the spawn location")
                .append(commandPrefix).append("set\n   &6Sets the spawn location");

        PlayerUtils.sendMessage(this.player, help.toString());
    }

}
