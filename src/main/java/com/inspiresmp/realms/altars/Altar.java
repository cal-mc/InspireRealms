package com.inspiresmp.realms.altars;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.config.AltarConfig;
import com.inspiresmp.realms.utils.ConfigUtils;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class Altar {

    public static YamlConfiguration altarConfig = AltarConfig.getFile();

    public static void generateDefaultSchematics() {
        new File(Main.instance.getDataFolder() + File.separator + "schematics").mkdirs();
        int schematicAmount = 5;
        String filePath = Main.instance.getDataFolder() + File.separator + "schematics" + File.separator + "Altar-";
        for (int i = 0; i < schematicAmount; i++) {
            ConfigUtils configUtils = new ConfigUtils(filePath + i + ".schematic");
            configUtils.createConfig("Altar-" + i + ".schematic");
        }
    }

    public static String getSchematicFile(int altarLevel) {
        if (altarConfig.getConfigurationSection("levels").getKeys(false) == null || altarConfig.getConfigurationSection("levels." + altarLevel) == null || altarConfig.get("levels." + altarLevel + ".schematic-file") == null)
            return null;
        return altarConfig.getString("levels." + altarLevel + ".schematic-file");
    }

}
