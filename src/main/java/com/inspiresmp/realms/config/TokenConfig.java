        package com.inspiresmp.realms.config;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.utils.ConfigUtils;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

        /**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class TokenConfig {

    // File path
    private static String filePath = Main.instance.getDataFolder() + File.separator + "tokens.yml";
    // Instance of the ConfigUtils class
    private static ConfigUtils configUtils = new ConfigUtils(filePath);

    /**
     * Gets the configuration file for a player using their UUID
     *
     * @return config file
     */
    public static YamlConfiguration getFile() {
        if (configUtils == null) return null;
        if (!configUtils.fileExists()) {
            configUtils.createConfig("token.yml");
        }
        return YamlConfiguration.loadConfiguration(new File(filePath));
    }

    /**
     * Save the configuration file
     *
     * @param config File to be saved
     */
    public boolean save(YamlConfiguration config) {
        try {
            config.save(new File(filePath));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}

