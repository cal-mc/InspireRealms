package com.inspiresmp.realms.config;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.utils.ConfigUtils;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class EntityConfig {
    // File path
    public static String filePath = Main.instance.getDataFolder() + File.separator + "entity.yml";

    /**
     * Gets the configuration file
     *
     * @return config file
     */
    public static YamlConfiguration getFile() {
        ConfigUtils configUtils = new ConfigUtils(filePath);
        if (!configUtils.fileExists()) {
            configUtils.createConfig("entity.yml");
        }
        return YamlConfiguration.loadConfiguration(new File(filePath));
    }

    /**
     * Save the configuration file
     *
     * @param config File to be saved
     */
    public static boolean save(YamlConfiguration config) {
        try {
            config.save(new File(filePath));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
