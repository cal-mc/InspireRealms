package com.inspiresmp.realms.config;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.utils.ConfigUtils;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class PlayerConfig {

    // File path
    public static String filePath = Main.instance.getDataFolder() + File.separator + "player-data" + File.separator;

    /**
     * Gets the configuration file for a player using their UUID
     *
     * @return config file
     */
    public static YamlConfiguration getFile(UUID player) {
        filePath = Main.instance.getDataFolder() + File.separator + "player-data" + File.separator + player.toString();
        ConfigUtils configUtils = new ConfigUtils(filePath);
        if (!configUtils.fileExists()) {
            configUtils.createConfig("player-data-template.yml");
        }
        return YamlConfiguration.loadConfiguration(new File(filePath));
    }

    /**
     * Save the configuration file
     *
     * @param config File to be saved
     */
    public static boolean save(YamlConfiguration config, UUID player) {
        filePath = Main.instance.getDataFolder() + File.separator + "player-data" + File.separator + player.toString();
        try {
            config.save(new File(filePath));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}

