package com.inspiresmp.realms.blocks;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.config.BlockConfig;
import com.inspiresmp.realms.menu.ItemFactory;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class BlockManager {

    // Used to store blocks for replacement
    private static HashMap<Location, String> replaceBlocks = new HashMap<>();
    // Instance of the BlockConfig class
    private static YamlConfiguration config = BlockConfig.getFile();
    // File path for config
    private String filePath = "worlds.";
    private String worldName = null;

    public BlockManager(World world) {
        this.worldName = world.getName();
        this.filePath = this.filePath + this.worldName + ".";
    }

    public BlockManager(String worldName) {
        this.worldName = worldName;
        this.filePath = this.filePath + this.worldName + ".";
    }

    /**
     * Gets the blocks to changed
     *
     * @return First Index: Location, Second Index: fromBlock
     */
    public static HashMap<Location, String> getBlocks() {
        return replaceBlocks;
    }

    /**
     * Adds a block to a location
     *
     * @param location The location of the block
     */
    public static void addBlock(Location location) {
        replaceBlocks.put(location, serializeBlock(location.getBlock()));
    }

    public static String serializeBlock(Block block){
        StringBuilder builder = new StringBuilder();
        builder.append(block.getType().toString()).append(";")
                .append(block.getData());
        return builder.toString();
    }


    /**
     * Removes a block from a location
     *
     * @param location The location of the block
     */
    public static void removeBlock(Location location) {
        if (getBlocks().containsKey(location)) {
            replaceBlocks.remove(location);
        }
    }

    /**
     * Check if a block has been changed in a location
     *
     * @param location The location to check
     * @return True: Block is there, False: Block is not there
     */
    public static boolean checkBlock(Location location) {
        return getBlocks().containsKey(location);
    }

    /**
     * Does the material require realms mode?
     *
     * @param material The material to check
     * @return True: Need realms, False: Doesn't need realms
     */
    public boolean requireSurvival(Material material){
        if(isNull() || config.get(filePath + "break." + material.toString() + ".requireSurvival") == null){
            return false;
        }
        return config.getBoolean(filePath + "break." + material.toString() + ".requireSurvival");
    }

    /**
     * Does the material require any tools?
     *
     * @param material The material to check
     * @return True: Need tools, False: Doesn't need tools
     */
    public boolean requireTools(Material material){
        if(isNull() || config.get(filePath + "break." + material.toString() + ".requireSurvival") == null){
            return false;
        }
        return config.getBoolean(filePath + "break." + material.toString() + ".requireSurvival");
    }

    /**
     * Gets a list of all the breakable material types
     *
     * @return List containing materials
     */
    public List<Material> getBreakableBlocks() {
        if (isNull() || config.get(this.filePath + "break") == null) {
            return null;
        }

        List<Material> materials = new ArrayList<>();
        for (String materialType : config.getConfigurationSection(this.filePath + "break").getKeys(false)) {
            materials.add(Material.getMaterial(materialType));
        }

        return materials;
    }

    /**
     * Gets a list of all the placeable material types
     *
     * @return List containing materials
     */
    public List<Material> getPlaceableBlocks() {
        if (isNull() || config.getStringList(this.filePath + "place") == null) {
            return null;
        }

        List<Material> materials = new ArrayList<>();
        for (String materialType : config.getStringList(this.filePath + "place")) {
            materials.add(Material.getMaterial(materialType));
        }

        return materials;
    }

    /**
     * Gets if blocks that are not on the placeable be allowed to placed or not
     *
     * @return True: Blocks cannot be placed, False: Blocks can be placed
     */
    public boolean isDisablePlace(){
        return !(isNull() || config.get(filePath + "disableBreak") == null) && config.getBoolean(filePath + "disableBreak");
    }

    /**
     * Gets if blocks that are not on the breakable be allowed to destroyed or not
     *
     * @return True: Blocks cannot be destroyed, False: Blocks can be destroyed
     */
    public boolean isDisableBreak(){
        return !(isNull() || config.get(filePath + "disableBreak") == null) && config.getBoolean(filePath + "disableBreak");
    }

    /**
     * The EXP to drop when a block is broke
     *
     * @param normal The default amount of EXP to drop
     * @return The amount of EXP to drop
     */
    public int getExpToDrop(int normal) {
        try {
            return config.getInt(this.filePath + "break.exp");
        } catch (Exception e) {
            return normal;
        }
    }

    /**
     * Get the block type to change a material to
     *
     * @param material The material to change from
     * @return The material to change to
     */
    public Material getChangedType(Material material) {
        if (isNull() || config.get(filePath + "break." + material.toString() + ".changeTo") == null) {
            return material;
        }
        return Material.getMaterial(config.getString(filePath + "break." + material.toString() + ".changeTo"));
    }

    /**
     * Get the drops from a material when broke
     *
     * @param material The material to get the drops of
     * @return A List<String> containing the drops in RAW form
     */
    public List<String> getDrops(Material material) {
        if (isNull() || config.getConfigurationSection(this.filePath + "break") == null || config.getStringList(this.filePath + "break." + material.toString() + ".items") == null) {
            return null;
        }
        return config.getStringList(this.filePath + "break." + material.toString() + ".items");
    }

    /**
     * Gets a List<String> of the items to drop
     *
     * @return The required items
     */
    public HashMap<ItemStack, Integer> getItemsToDrop(Material type) {
        if (isNull() || config.getConfigurationSection(this.filePath + "break") == null || config.getStringList(this.filePath + "break." + type.toString() + ".items") == null) {
            return null;
        }
        HashMap<ItemStack, Integer> drops = new HashMap<>();
        for (String itemString : getDrops(type)) {
            try {
                String matIndex = ";";
                if (itemString.contains("#")) {
                    matIndex = "#";
                }
                int chance = Integer.parseInt(itemString.split(";")[1].split(":")[0]);
                Material material = Material.getMaterial(itemString.split(matIndex)[0]);
                int data = Integer.parseInt(itemString.split("#")[1].split(";")[0]);
                int min = Integer.parseInt(itemString.split(":")[1].split("-")[0]);
                int max = Integer.parseInt(itemString.split(":")[1].split("-")[1]);
                drops.put(new ItemFactory(material, Main.instance.random.nextInt((max - min) + 1) + min).setData(data).getItem(), chance);
            } catch (Exception e) {
                Main.instance.getLogger().log(Level.WARNING, "Block config error at: " + itemString);
            }
        }
        return drops;
    }

    /**
     * Gets a List<ItemStack> of the required resources to upgrade the realm
     *
     * @return The required items
     */
    public List<ItemStack> getRawItemsToDrop(Material type) {
        if (isNull() || config.getConfigurationSection(this.filePath + "break") == null || getDrops(type) == null || getDrops(type).isEmpty()) {
            return null;
        }

        List<ItemStack> drops = new ArrayList<>();
        for (String itemString : getDrops(type)) {
            try {
                String matIndex = ";";
                if (itemString.contains("#")) {
                    matIndex = "#";
                }
                Material material = Material.getMaterial(itemString.split(matIndex)[0]);
                int data = Integer.parseInt(itemString.split("#")[1].split(";")[0]);
                int min = Integer.parseInt(itemString.split(":")[1].split("-")[0]);
                int max = Integer.parseInt(itemString.split(":")[1].split("-")[1]);
                drops.add(new ItemFactory(material).setData(data).setAmount(Main.instance.random.nextInt((max - min) + 1) + min).getItem());
            } catch (Exception e) {
                Main.instance.getLogger().log(Level.WARNING, "Block config error at: " + itemString);
            }
        }
        return drops;
    }

    /**
     * Gets a random item to drop
     *
     * @return The random item
     */
    public ItemStack getRandomItemToDrop(Material blockType) {
        if (isNull() || getRawItemsToDrop(blockType) == null || getRawItemsToDrop(blockType).isEmpty()) {
            return null;
        }
        HashMap<ItemStack, Integer> drops = getItemsToDrop(blockType);
        int range = 0;
        for (int chance : drops.values()) {
            range = range + chance;
        }

        ItemStack item;
        List<ItemStack> items = new ArrayList<>();

        for (Map.Entry<ItemStack, Integer> map : drops.entrySet()) {
            item = map.getKey();
            int itemValue = map.getValue();
            for (int i = 0; i < itemValue; i++) {
                items.add(item);
            }
        }

        return items.get(Main.instance.random.nextInt(items.size()));
    }

    /**
     * Get a list of tools required to break a block
     *
     * @param type The material to get the tools of
     * @return List of tools required to break the blocks
     */
    public List<Material> getTools(Material type) {
        if (isNull() || config.getConfigurationSection(this.filePath + "break") == null || config.getStringList(this.filePath + "break." + type.toString() + ".tools") == null) {
            return null;
        }

        List<Material> materials = new ArrayList<>();

        for (String materialName : config.getStringList(this.filePath + "break." + type.toString() + ".tools")) {
            materials.add(Material.getMaterial(materialName));
        }

        return materials;
    }

    /**
     * The time required to reset a material back to normal
     *
     * @param material The material to get the reset time of
     * @return The reset time, default is 0 seconds
     */
    public int getResetTime(Material material) {
        if (isNull() || config.get(this.filePath + "break." + material.toString() + ".resetTime") == null) {
            return 0;
        }

        return config.getInt(this.filePath + "break." + material.toString() + ".resetTime");
    }

    /**
     * Is the config file for this world null?
     *
     * @return True: Not null, False: Is null
     */
    public boolean isNull() {
        return (config.get("worlds." + this.worldName) == null);
    }

    /**
     * Start a block reset timer to replace changed blocks
     *
     * @param delay The time in which to delay the action by
     * @param location The location of the block
     */
    public static void startBlockResetTimer(int delay, final Location location) {
        new BukkitRunnable() {
            public void run() {
                this.cancel();
                if (!getBlocks().containsKey(location)) {
                    return;
                }
                String serailizedBlock = getBlocks().get(location);
                Material blockType = Material.getMaterial(serailizedBlock.split(";")[0]);
                Byte blockData = (byte) Integer.parseInt(serailizedBlock.split(";")[1]);

                location.getBlock().setType(blockType);
                location.getBlock().setData(blockData);

                replaceBlocks.remove(location);
            }
        }.runTaskLater(Main.instance, delay * 20L);
    }

    /**
     * Clear out all of the changed blocks and reset them
     */
    public static void flushBlocks() {
        HashMap<Location, String> blocks = new HashMap<>(getBlocks());
        for (Location location : blocks.keySet()) {
            String serailizedBlock = getBlocks().get(location);
            Material blockType = Material.getMaterial(serailizedBlock.split(";")[0]);
            Byte blockData = (byte) Integer.parseInt(serailizedBlock.split(";")[1]);

            location.getBlock().setType(blockType);
            location.getBlock().setData(blockData);

            replaceBlocks.remove(location);
        }
    }

}
