package com.inspiresmp.realms.realms;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.config.RealmConfig;
import com.inspiresmp.realms.tokens.TokenManager;
import com.inspiresmp.realms.utils.GameUtils;
import com.inspiresmp.realms.utils.PlayerUtils;
import com.inspiresmp.realms.utils.SimpleScoreboard;
import com.inspiresmp.realms.utils.WorldUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.logging.Level;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class RealmManager {

    // Instance of the RealmManager class
    private static RealmManager manager = new RealmManager();

    // List of all of the realms in use
    private static HashMap<UUID, Realm> realms = new HashMap<>();

    /**
     * Gets the instance of the RealmManager class
     *
     * @return RealmManager class
     */
    public static RealmManager getManager() {
        return manager;
    }

    /**
     * Teleport the player to a realm
     *
     * @param player The player to be teleported to the realm
     * @param realm  The realm to teleport the player to
     * @return True: They were teleported, False: They were not teleported
     */
    public boolean teleport(Player player, Realm realm) {
        if (!realm.hasRealm()) {
            PlayerUtils.sendMessage(player, "&cThat realm could not be found!");
            return false;
        }

        if (realm.getPlayers().contains(player.getUniqueId())) {
            PlayerUtils.sendMessage(player, "&cYou are already in this realm!");
            return false;
        }

        if (!WorldUtils.isLoaded(realm.getRealmWorld())) {
            PlayerUtils.sendMessage(player, "&b&lLoading realm...");
            WorldUtils.loadWorld(realm.getOwner().toString());
        }

        // Update the players
        realm.updatePlayers();

        player.teleport(realm.getRealmSpawn());

        player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 3 * 20, 6));
        PlayerUtils.sendMessage(player, "&bWelcome to &6&l" + Bukkit.getOfflinePlayer(realm.getOwner()).getName() + "'s &brealm!");


        return true;
    }

    /**
     * Get the realm for a player
     *
     * @param player The player you want to get the realm of
     * @return The players realm
     */
    public Realm getPlayerRealm(Player player) {
        return getPlayerRealm(player.getUniqueId());
    }

    /**
     * Get the realm for a player using their UUID
     *
     * @param playerUUID The player you want to get the realm of using their UUID
     * @return The players realm
     */
    public Realm getPlayerRealm(UUID playerUUID) {
        return (isRealmLoaded(playerUUID)) ? realms.get(playerUUID) : new Realm(playerUUID);
    }

    public boolean isRealmLoaded(UUID player) {
        return realms.containsKey(player);
    }

    /**
     * Adds a realm from the realms list
     *
     * @param player The players realm to add
     * @return True: Added, False: Not added
     */
    public boolean addRealm(UUID player, Realm realm) {
        if (realms.containsKey(player)) {
            return false;
        }

        realms.put(player, realm);
        return true;
    }

    /**
     * Removes a realm from the realms list
     *
     * @param player The players realm to remove
     * @return True: Removed, False: Not removed
     */
    public boolean removeRealm(UUID player) {
        if (!realms.containsKey(player)) {
            return false;
        }

        realms.remove(player);
        return true;
    }

    /**
     * Unloads a realm from memory
     *
     * @param realm The realm to unload
     * @return True: Realm Unloaded, False: Realm not unloaded
     */
    public boolean unloadRealm(Realm realm) {
        if (!realm.hasRealm()) {
            Main.instance.getLogger().log(Level.WARNING, "Failed Unloaded " + Bukkit.getOfflinePlayer(realm.getOwner()).getName() + "'s realm!");
            return false;
        }

        if (realm.getRealmWorld() != null && !Bukkit.getWorlds().contains(realm.getRealmWorld())) {
            return false;
        }

        World realmWorld = realm.getRealmWorld();
        World spawnWorld = GameUtils.getSpawnWorld();
        if (realmWorld != null && realmWorld.getPlayers() != null && !realmWorld.getPlayers().isEmpty()) {
            for (Player realmPlayer : realmWorld.getPlayers()) {
                if (spawnWorld == null) {
                    spawnWorld = Bukkit.getWorlds().get(0);
                }
                PlayerUtils.sendMessage(realmPlayer, "&cThe realm that you were in was unloaded, you were moved to spawn!");
                realmPlayer.teleport(spawnWorld.getSpawnLocation());
            }
        }

        removeRealm(realm.getOwner());
        WorldUtils.unloadWorld(realm.getRealmWorld());
        Main.instance.getLogger().log(Level.INFO, "Unloaded " + Bukkit.getOfflinePlayer(realm.getOwner()).getName() + "'s realm!");

        return true;
    }

    /**
     * Gets all of the realms that are in use
     *
     * @return A collection of realms
     */
    public Collection<Realm> getRealms() {
        return realms.values();
    }

    /**
     * Displays a players statistics including realm information
     *
     * @param realm  The realm to query
     * @param player The player to query
     */
    public void displayStats(final Realm realm, final Player player) {
        final SimpleScoreboard playerScoreboard = new SimpleScoreboard("&6Your stats");
        playerScoreboard
                .add("&aBlocks mined:")
                .add("  &r&lgetBlocks()")
                .add("&aTokens")
                .add("  &r&l" + TokenManager.getTokens(player.getUniqueId()))
                .add("&aAltar health:")
                .add("  &r&l" + Math.round(player.getHealth()))
                .add("&aAltar level:")
                .add("  &r&l" + player.getLevel());
        playerScoreboard.build();
        playerScoreboard.send(player);
        new BukkitRunnable() {
            @Override
            public void run() {
                player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
                playerScoreboard.reset();
                playerScoreboard.unregister();
            }
        }.runTaskLater(Main.instance, 20 * 30);
    }

    /**
     * Gets all of the accessible realms for a player
     *
     * @param player The player to query
     * @return List<String> of accessible realms
     */
    public List<String> getAccessibleRealms(Player player) {
        YamlConfiguration config = RealmConfig.getFile();
        List<String> access = new ArrayList<>();
        List<String> members;
        for (String realmName : config.getKeys(false)) {
            if (config.getStringList(realmName + ".members") != null) {
                members = config.getStringList(realmName + ".members");
                if (members.contains(player.getUniqueId().toString())) {
                    access.add(realmName);
                }
            }
        }

        return access;

    }

    public boolean isInPlayerRealm(Player player, String realmName) {
        return getAccessibleRealms(player).contains(realmName);
    }


}
