package com.inspiresmp.realms.realms;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.altars.Altar;
import com.inspiresmp.realms.config.AltarConfig;
import com.inspiresmp.realms.config.RealmConfig;
import com.inspiresmp.realms.utils.GameUtils;
import com.inspiresmp.realms.utils.PlayerUtils;
import com.inspiresmp.realms.utils.WorldEditUtils;
import com.inspiresmp.realms.utils.WorldUtils;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class Realm {

    // List of all the players within the realm
    private List<UUID> players = new ArrayList<>();
    // List of all of the members of the realm
    private List<UUID> members = new ArrayList<>();
    // UUID of the realm owner
    private UUID ownerUUID;
    // Which player the realm belongs to
    private Player player;
    // Realm world
    private World realmWorld = null;
    // Realm spawn location
    private Location realmSpawn = null;
    // The level of the realm
    private int level = 0;
    // The size of the realm
    private int size = 0;

    /**
     * Create an instance of the Realms class
     *
     * @param player Owner of the realm
     */
    public Realm(Player player) {
        RealmManager.getManager().addRealm(player.getUniqueId(), this);
        this.player = player;
        this.ownerUUID = player.getUniqueId();
        getRealmSpawn();
        getMembers();
    }

    public Realm(UUID playerUUID){
        RealmManager.getManager().addRealm(playerUUID, this);
        if(Bukkit.getOfflinePlayer(playerUUID).isOnline()){
            this.player = Bukkit.getPlayer(playerUUID);
        }
        this.ownerUUID = playerUUID;
        getRealmSpawn();
        getMembers();
    }

    public Realm(OfflinePlayer player) {
        if (player.isOnline()) {
            this.player = player.getPlayer();
        }
        this.ownerUUID = player.getUniqueId();
        getRealmSpawn();
        getMembers();
    }

    /**
     * Creates a realm for the player
     *
     * @return True: Realm was created, False: Realm was not created
     */
    public boolean createRealm() {
        YamlConfiguration realmConfig = RealmConfig.getFile();
        if (hasRealm()) {
            PlayerUtils.sendMessage(this.player, "&cYou already have a realm!");
            return false;
        }

        PlayerUtils.sendMessage(player, "&b&lCreating your realm...");

        WorldCreator realmSettings = new WorldCreator(getOwner().toString());

        World realmWorld = Bukkit.createWorld(realmSettings);

        Location baseLocation = realmWorld.getHighestBlockAt(0, 0).getLocation();

        int altarOffsetX = 0;
        int altarOffsetY = 0;
        int altarOffsetZ = 0;

        YamlConfiguration altarConfig = AltarConfig.getFile();
        try {
            altarOffsetX = altarConfig.getInt("offset.x");
            altarOffsetY = altarConfig.getInt("offset.y");
            altarOffsetZ = altarConfig.getInt("offset.z");
        } catch (Exception e) {
            Main.instance.getLogger().log(Level.WARNING, "Altar offset is invalid in the altar.yml file!");
        }

        new WorldEditUtils().loadSchematic(Altar.getSchematicFile(1), realmWorld, baseLocation.clone().add(altarOffsetX, altarOffsetY, altarOffsetZ));


        ConfigurationSection section = realmConfig.createSection(getOwner().toString());
        this.level = RealmConfig.getFile().getInt("defaultLevel");;
        section.set("level", this.level);
        section.set("altarLocation", GameUtils.locationToString(baseLocation.clone().add(altarOffsetX,altarOffsetY,altarOffsetZ)));
        placeAltar(this.level);
        section.set("owner", this.ownerUUID.toString());
        section.set("worldName", realmWorld.getName());
        this.realmSpawn = baseLocation.clone().add(0, 1, 0);
        section.set("spawnLocation", GameUtils.locationToString(this.realmSpawn));
        this.size = AltarConfig.getFile().getInt("levels." + level + ".size");
        section.set("size", this.size);
        section.set("resourcesRequired", AltarConfig.getFile().getStringList("levels." + this.level + ".items-required"));
        List<String> members = new ArrayList<>();
        members.add(getOwner().toString());
        section.set("members", members);
        // TODO: Add in realm to next and required to level up the next realm level from the global config
        RealmConfig.save(realmConfig);

        RealmManager.getManager().teleport(this.player, this);
        WorldUtils.setWorldborder(this.player, this.size);

        PlayerUtils.sendMessage(this.player, "&b&lRealm creation complete!");

        return true;
    }

    public void placeAltar(int level){
        if(!hasRealm()){
            return;
        }
        Location altarLocation = GameUtils.stringToLocation(RealmConfig.getFile().getString(getOwner().toString() + ".altarLocation"));

        new WorldEditUtils().loadSchematic(Altar.getSchematicFile(level), getRealmWorld(), altarLocation);
    }

    /**
     * Purges the players realm
     *
     * @return True: Realm was purged, False: Realm was not purged
     */
    public boolean purgeRealm() {
        YamlConfiguration realmConfig = RealmConfig.getFile();
        if (!hasRealm()) {
            PlayerUtils.sendMessage(this.player, "&cYou do not have a realm!");
            return false;
        }

        PlayerUtils.sendMessage(this.player, "&b&lPurging your realm...");

        File realmWorldFile = getRealmWorld().getWorldFolder();

        RealmManager.getManager().unloadRealm(this);
        WorldUtils.deleteWorld(realmWorldFile);

        realmConfig.set(this.player.getUniqueId().toString(), null);
        RealmConfig.save(realmConfig);

        PlayerUtils.sendMessage(this.player, "&b&lRealm purge complete!");

        return true;
    }

    /**
     * Gets the realm world
     *
     * @return Realm world
     */
    public World getRealmWorld() {
        if (this.realmWorld == null || this.realmWorld != Bukkit.getWorld(getOwner().toString())) {
            this.realmWorld = Bukkit.createWorld(new WorldCreator(getOwner().toString()));
        }

        return this.realmWorld;
    }

    /**
     * Gets the realm spawn
     *
     * @return Realm spawn
     */
    public Location getRealmSpawn() {
        YamlConfiguration realmConfig = RealmConfig.getFile();
        if (!hasRealm()) {
            return null;
        }

        if (realmConfig.get(getOwner().toString() + ".spawnLocation") != null && this.realmSpawn != GameUtils.stringToLocation(realmConfig.getString(getOwner().toString() + ".spawnLocation"))) {
            this.realmSpawn = GameUtils.stringToLocation(realmConfig.getString(getOwner().toString() + ".spawnLocation"));
        }

        return this.realmSpawn;
    }

    public UUID getOwner() {
        return this.ownerUUID;
    }

    /**
     * Check to see if the player has a realm
     *
     * @return True: Player has a realm, False: Player does not have a realm
     */
    public boolean hasRealm() {
        YamlConfiguration realmConfig = RealmConfig.getFile();

        return (realmConfig.getConfigurationSection(ownerUUID.toString()) != null);
    }

    /**
     * Summons a player to the realm
     *
     * @param toSummon The player to be summoned to the realm
     * @return True: Summoned, False: Not summoned
     */
    public boolean summonPlayer(Player toSummon) {
        if (!toSummon.isOnline()) {
            PlayerUtils.sendMessage(player, "&cTarget player &7[" + toSummon.getName() + "] &c could not be found!");
            return false;
        }
        if (!hasRealm()) {
            PlayerUtils.sendMessage(player, "&cThat realm could not be found!");
            return false;
        }

        RealmManager.getManager().teleport(toSummon, this);

        return true;
    }

    public boolean invitePlayer(Player player) {
        if (!hasRealm()) {
            return false;
        }

        if (getMembers().contains(player.getUniqueId())) {
            PlayerUtils.sendMessage(player, "&cYou are already in this realm!");
            return false;
        }

        YamlConfiguration realmConfig = RealmConfig.getFile();

        List<String> members = new ArrayList<>();
        for (UUID memberUUID : getMembers()) {
            members.add(memberUUID.toString());
        }

        members.add(player.getUniqueId().toString());

        realmConfig.set(getOwner().toString() + ".members", members);

        RealmConfig.save(realmConfig);
        return true;
    }

    public boolean invitePlayer(UUID player) {
        return invitePlayer(Bukkit.getPlayer(player));
    }

    public boolean kickPlayer(Player player) {
        if (!hasRealm()) {
            return false;
        }

        if (!getMembers().contains(player.getUniqueId())) {
            PlayerUtils.sendMessage(this.player, "&7&l" + player.getName() + " &cis not in this realm!");
            return false;
        }

        YamlConfiguration realmConfig = RealmConfig.getFile();

        List<String> members = new ArrayList<>();
        for (UUID memberUUID : getMembers()) {
            members.add(memberUUID.toString());
        }

        members.remove(player.getUniqueId().toString());

        realmConfig.set(getOwner().toString() + ".members", members);

        RealmConfig.save(realmConfig);

        return true;
    }

    /**
     * Kicks a player from the realm list
     *
     * @param player The player to kick from the realm
     * @return True: Player was kicked, False: Player was not kicked
     */
    public boolean kickPlayer(UUID player) {
        return kickPlayer(Bukkit.getPlayer(player));
    }

    /**
     * Gets the members that are allowed to build in realm
     *
     * @return List of members
     */
    public List<UUID> getMembers() {
        YamlConfiguration realmConfig = RealmConfig.getFile();
        if (!hasRealm()) {
            return null;
        }

        if (realmConfig.get(getOwner().toString() + ".members") != null) {
            for (String configMember : realmConfig.getStringList(getOwner().toString() + ".members")) {
                if (!this.members.contains(UUID.fromString(configMember))) {
                    this.members.add(UUID.fromString(configMember));
                }
            }
        }

        return this.members;
    }

    /**
     * Removes a player to the realm's player list
     *
     * @param player The player to remove to the realms player list
     * @return True: Added-, False: Not added
     */
    public boolean removePlayer(Player player) {
        return removePlayer(player.getUniqueId());
    }

    /**
     * Removes a player to the realm's player list using their UUID
     *
     * @param player The player to remove to the realms player list using their UUID
     * @return True: Added, False: Not added
     */
    public boolean removePlayer(UUID player) {
        if (!players.contains(player)) {
            return false;
        }

        players.remove(player);
        return true;
    }

    /**
     * Gets a list of all of the players in the realm
     *
     * @return List of players in realm
     */
    public List<UUID> getPlayers() {
        return this.players;
    }

    /**
     * Updates the players list of the realm
     */
    public void updatePlayers() {
        if (getPlayers() == null || getRealmWorld() == null) {
            return;
        }
        for (UUID pUUID : new ArrayList<>(getPlayers())) {
            getPlayers().remove(pUUID);
        }
        for (Player player : getRealmWorld().getPlayers()) {
            players.add(player.getUniqueId());
        }
    }

    public boolean setLevel(int level) {
        if (!hasRealm()) {
            return false;
        }
        this.level = level;
        RealmConfig.getFile().set(this.ownerUUID.toString() + ".level", this.level);
        return true;
    }

    public int getLevel() {
        if (!hasRealm()) {
            return level;
        }

        YamlConfiguration config = RealmConfig.getFile();
        if (config.get(this.ownerUUID.toString() + ".level") == null || this.level == config.getInt(this.ownerUUID.toString() + ".level")) {
            return this.level;
        }

        this.level = config.getInt(this.ownerUUID.toString() + ".level");
        return this.level;
    }

    public boolean upgradeRealm(boolean fromMenu) {
        if (!hasRealm()) {
            PlayerUtils.sendMessage(this.player, "&cYou do not have realm!");
            return false;
        }

        if(!this.player.getWorld().getName().equals(getOwner().toString())) {
            PlayerUtils.sendMessage(this.player, "&cYou must be in your realm to upgrade your realm!");
            return false;
        }

        YamlConfiguration config = RealmConfig.getFile();
        YamlConfiguration altarConfig = AltarConfig.getFile();

        int maxLevel = 0;

        for(String level : altarConfig.getConfigurationSection("levels").getKeys(false)){
            maxLevel = Integer.parseInt(level);
        }

        if(getLevel() + 1 > maxLevel){
            PlayerUtils.sendMessage(this.player, "&aYour realm is already maxed out!");
            return false;
        }

        this.level = getLevel() + 1;
        this.size = altarConfig.getInt("levels." + this.level + ".size");
        config.set(getOwner().toString() + ".level", this.level);
        config.set(getOwner().toString() + ".resourcesRequired", altarConfig.getStringList("levels." + this.level + ".items-required"));
        config.set(getOwner().toString() + ".size", this.size);
        System.out.print(this.size + "");
        RealmConfig.save(config);
        WorldUtils.setWorldborder(player, this.size);
        placeAltar(this.level);
        this.player.teleport(WorldUtils.getSpawnLocation());
        PlayerUtils.sendMessage(this.player, "&b&lYour realm has been upgraded!");
        RealmManager.getManager().teleport(this.player, this);

        if(fromMenu){
            RealmGUI.openUpgradeGUI(player);
        }

        return true;
    }

    /**
     * Gets a List<String> of the required resources to upgrade the realm
     *
     * @return The required items
     */
    public List<String> getRawRequiredResources() {
        if (!hasRealm()) {
            PlayerUtils.sendMessage(this.player, "&cYou do not have a realm!");
            return null;
        }

        YamlConfiguration config = RealmConfig.getFile();

        if (config.getStringList(this.ownerUUID.toString() + ".resourcesRequired") == null) {
            return null;
        }

        return new ArrayList<>(config.getStringList(this.ownerUUID.toString() + ".resourcesRequired"));
    }

    /**
     * Gets a List<ItemStack> of the required resources to upgrade the realm
     *
     * @return The required items
     */
    public List<ItemStack> getRequiredResources() {
        if (!hasRealm()) {
            PlayerUtils.sendMessage(this.player, "&cYou do not have a realm!");
            return null;
        }

        if (getRawRequiredResources() == null || getRawRequiredResources().isEmpty()) {
            return null;
        }

        List<String> resources = new ArrayList<>(getRawRequiredResources());
        List<ItemStack> requiredItems = new ArrayList<>();

        for (String required : resources) {
            try {
                Material type = Material.getMaterial(required.split(":")[0]);
                int amount = Integer.parseInt(required.split(":")[1]);
                requiredItems.add(new ItemStack(type, amount));
            } catch (Exception e) {
                Main.instance.getLogger().log(Level.WARNING, "Failed to get required resource: " + required);
            }
        }

        return requiredItems;
    }

    /**
     * Removes a item from the resource list
     *
     * @param type Material
     * @param amount How many to remove
     * @return True: Added resource, False: Did not add resource
     */
    public boolean addResource(Material type, int amount){
        YamlConfiguration config = RealmConfig.getFile();

        List<ItemStack> items = getRequiredResources();
        List<String> rawItems = new ArrayList<>();

        for(ItemStack item : new ArrayList<>(items)){
            if(item.getType() == type && item.getAmount() > amount){
                items.remove(item);
                player.getInventory().removeItem(new ItemStack(type));
                player.getInventory().addItem(new ItemStack(type, item.getAmount() - amount));
                rawItems.add(item.getType() + ":" + (item.getAmount() - amount));
                amount = 0;
                continue;
            } else if(item.getType() == type && item.getAmount() <= amount){
                player.getInventory().removeItem(new ItemStack(type));
                items.remove(item);
                continue;
            }
            rawItems.add(item.getType() + ":" + item.getAmount());
        }

        if(rawItems.isEmpty()){
            player.closeInventory();
            return upgradeRealm(true);
        }

        config.set(this.ownerUUID.toString() + ".resourcesRequired", rawItems);
        RealmConfig.save(config);

        RealmGUI.openUpgradeGUI(player);
        return true;
    }

}

