package com.inspiresmp.realms.realms;

import com.inspiresmp.realms.menu.InventoryMenu;
import com.inspiresmp.realms.menu.InventoryMenuItem;
import com.inspiresmp.realms.menu.InventoryUtils;
import com.inspiresmp.realms.menu.ItemFactory;
import com.inspiresmp.realms.utils.PlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.UUID;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class  RealmGUI {

    public static final String LIST_TITLE = ChatColor.translateAlternateColorCodes('&',"&lYour accessible realms");
    public static final String PLAYER_TITLE = ChatColor.translateAlternateColorCodes('&',"&lRealm Members");
    public static final String UPGRADE_TITLE = ChatColor.translateAlternateColorCodes('&',"&lRealm upgrade");

    /**
     * Open the realms list that displays which realms a player can access
     *
     * @param player Player to open the realm menu for
     * @return True: Opened, False: Did not open
     */
    public static boolean openListGUI(Player player) {
        if (RealmManager.getManager().getAccessibleRealms(player) == null || RealmManager.getManager().getAccessibleRealms(player).isEmpty()) {
            PlayerUtils.sendMessage(player, "&cYou do not have access to any realms!");
            return false;
        }

        List<String> accessible = RealmManager.getManager().getAccessibleRealms(player);

        final InventoryMenu gui = new InventoryMenu(LIST_TITLE, InventoryUtils.getClosestNine(accessible.size()));
        int index = 0;

        for (String realmName : accessible) {
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(UUID.fromString(realmName));
            final Realm ownerRealm = new Realm(offlinePlayer);
            String itemName = "&7&l" + offlinePlayer.getName() + "'s &brealm";
            if (player.getUniqueId().equals(ownerRealm.getOwner())) {
                itemName = "&bYour realm";
            }
            ItemStack guiItem = new ItemFactory(Material.SKULL_ITEM, 1, 3).setName(itemName).setLore("&bClick to visit realm!").setSkullOwner(offlinePlayer.getName()).getItem();
            gui.setItem(new InventoryMenuItem(guiItem) {
                @Override
                protected void onClick(Player player, boolean isRightClick, boolean isShiftClick) {
                    if (RealmManager.getManager().teleport(player, ownerRealm)) {
                        gui.closeMenu(player);
                    }
                }
            }, index);
            index++;
        }
        gui.openMenu(player);
        return true;
    }

    /**
     * Opens up a display that contains a list of players that can access your realm
     *
     * @param player The player to open the menu for
     * @return True: Opened, False: Did not open
     */
    public static boolean openPlayerGUI(Player player) {
        Realm realm = RealmManager.getManager().getPlayerRealm(player);
        if (!realm.hasRealm()) {
            PlayerUtils.sendMessage(player, "&cYou do not have a realm!");
            return false;
        }

        List<UUID> members = realm.getMembers();

        final InventoryMenu gui = new InventoryMenu(PLAYER_TITLE, InventoryUtils.getClosestNine(members.size()));
        int index = 0;

        for (UUID realmMember : members) {
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(realmMember);
            final Realm ownerRealm = new Realm(offlinePlayer);
            ItemFactory guiItem = new ItemFactory(Material.SKULL_ITEM, 1, 3).setName("&7&l" + offlinePlayer.getName()).setSkullOwner(offlinePlayer.getName());
            if (ownerRealm.getMembers() != null && ownerRealm.getMembers().contains(player.getUniqueId())) {
                guiItem.setLore("&bClick to visit!");
            }
            gui.setItem(new InventoryMenuItem(guiItem.getItem()) {
                @Override
                protected void onClick(Player player, boolean isRightClick, boolean isShiftClick) {
                    if (ownerRealm.hasRealm() && ownerRealm.getMembers().contains(player.getUniqueId())) {
                        RealmManager.getManager().teleport(player, ownerRealm);
                    }
                }
            }, index);
            index++;
        }
        gui.openMenu(player);
        return true;
    }

    public static boolean openUpgradeGUI(Player player) {
        Realm realm = RealmManager.getManager().getPlayerRealm(player);
        if (!realm.hasRealm()) {
            PlayerUtils.sendMessage(player, "&cYou do not have a realm!");
            return false;
        }

        List<ItemStack> items = realm.getRequiredResources();

        final InventoryMenu gui = new InventoryMenu(UPGRADE_TITLE, InventoryUtils.getClosestNine(items.size()));
        int index = 0;

        for (final ItemStack guiItem : items) {
            gui.setItem(new InventoryMenuItem(guiItem) {
                @Override
                protected void onClick(Player player, boolean isRightClick, boolean isShiftClick) {

                }
            }, index);
            index++;
        }
        gui.openMenu(player);
        return true;
    }

}
