package com.inspiresmp.realms.tutorial;


import com.inspiresmp.realms.config.PlayerConfig;
import com.inspiresmp.realms.config.TokenConfig;
import com.inspiresmp.realms.config.TutorialConfig;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.UUID;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class TutorialManager {

    private static YamlConfiguration tutorialConfig = TokenConfig.getFile();

    // File path
    private String filePath = "tutorial.";

    private int floor;
    private String tutorialName = null;

    public TutorialManager(int floor) {
        this.floor = floor;
        this.tutorialName = this.getTutorialName();
        filePath = filePath + floor + ".";
    }

    /**
     * Sets the current floor
     *
     * @return Instance of TutorialManager class
     */
    public TutorialManager setFloor(int floor) {
        this.floor = floor;
        return this;
    }

    /**
     * Gets the current floor
     *
     * @return The floor
     */
    public int getFloor() {
        return this.floor;
    }

    /**
     * Sets the name of the tutorial floor
     *
     * @param tutorialName The name for the tutorial floor
     * @return Instance of the TutorialManager class
     */
    public TutorialManager setTutorialName(String tutorialName) {
        this.tutorialName = tutorialName;
        tutorialConfig.set(filePath + "name", this.tutorialName);
        TutorialConfig.save(tutorialConfig);
        return this;
    }

    /**
     * Gets the tutorial floor name
     *
     * @return The tutorial floor name
     */
    public String getTutorialName(){
        if(this.tutorialName == null){
            this.tutorialName = (tutorialConfig.get(filePath + "name") != null) ? tutorialConfig.getString(filePath + "name") : null;
        }
        return this.tutorialName;
    }



    /**
     * Gets the current floor a player is on in the tutorial
     *
     * @param player The player to get the information for
     * @return THe current floor
     */
    public static int getPlayerFloor(UUID player) {
        int floor = 0;
        YamlConfiguration playerConfig = PlayerConfig.getFile(player);
        if (playerConfig.get("tutorial.floor") == null) {
            return floor;
        }
        floor = playerConfig.getInt("tutorial.floor");
        return floor;
    }

    /**
     * Sets the current floor a player is on in the tutorial
     *
     * @param player The player to get the information for
     * @return THe current floor
     */
    public static boolean setPlayerFloor(UUID player, int floor) {
        YamlConfiguration playerConfig = PlayerConfig.getFile(player);
        playerConfig.set("tutorial.floor", floor);
        return PlayerConfig.save(playerConfig, player);
    }
}
