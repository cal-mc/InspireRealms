package com.inspiresmp.realms.listeners;

import com.inspiresmp.realms.blocks.BlockManager;
import com.inspiresmp.realms.config.BlockConfig;
import com.inspiresmp.realms.realms.RealmManager;
import com.inspiresmp.realms.utils.messages.Message;
import com.inspiresmp.realms.utils.messages.MessageType;
import net.minecraft.server.v1_8_R1.BlockPosition;
import net.minecraft.server.v1_8_R1.PacketPlayOutBlockBreakAnimation;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R1.CraftServer;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class BlockListener implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        if(player.getWorld().getName().equalsIgnoreCase(RealmManager.getManager().getPlayerRealm(player).getRealmWorld().getName())) {
            event.setCancelled(false);
            return;
        }
        if(RealmManager.getManager().isInPlayerRealm(player, player.getWorld().getName())){
            event.setCancelled(false);
            return;
        }
        Block block = event.getBlock();
        Location blockLocation = block.getLocation();
        if (player.hasPermission("inspiresurvival.block.override")) {
            BlockManager.removeBlock(blockLocation);
            return;
        }

        if (BlockManager.checkBlock(blockLocation)) {
            event.setCancelled(true);
            return;
        }

        if(!player.hasPermission("inspiresurvival.block.place." + player.getWorld().getName()) || !RealmManager.getManager().getPlayerRealm(player).getMembers().contains(player.getUniqueId())) {
            new Message(BlockConfig.getFile().getString("blockMessage").replace("[action]", "place")).send(player, MessageType.HOTBAR);
            event.setCancelled(true);
            return;
        }

        BlockManager blockManager = new BlockManager(blockLocation.getWorld().getName());
        if (blockManager.isNull() || blockManager.getTools(block.getType()) == null || blockManager.getBreakableBlocks() == null || (!blockManager.getPlaceableBlocks().contains(event.getBlock().getType())) && blockManager.isDisablePlace()) {
            event.setCancelled(true);
            return;
        }

        if (BlockConfig.getFile().get("blockMessage") != null && BlockConfig.getFile().get("notifyPlayer") != null && BlockConfig.getFile().getBoolean("notifyPlayer")) {
            new Message(BlockConfig.getFile().getString("blockMessage").replace("[action]", "place")).send(player, MessageType.HOTBAR);
            event.setCancelled(true);
        }

    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if(player.getWorld().getName().equalsIgnoreCase(RealmManager.getManager().getPlayerRealm(player).getRealmWorld().getName())) {
            return;
        }
        if(RealmManager.getManager().isInPlayerRealm(player, player.getWorld().getName())){
            event.setCancelled(false);
            return;
        }

        Block block = event.getBlock();
        BlockManager blockManager = new BlockManager(player.getWorld().getName());
        /** If the player has overrider perms and is in creative mode allow them to break blocks if it does require realms to break it **/
        if (player.hasPermission("inspiresurvival.block.override") && player.getGameMode() != GameMode.SURVIVAL && (blockManager.requireSurvival(block.getType()) || blockManager.isDisableBreak())) {
            BlockManager.removeBlock(block.getLocation());
            return;
        }

        /** Called to check if the blocks are null **/
        if (BlockManager.checkBlock(block.getLocation()) || block.getType() == Material.AIR || blockManager.isNull() || blockManager.getTools(block.getType()) == null || blockManager.getBreakableBlocks() == null || (!blockManager.getBreakableBlocks().contains(event.getBlock().getType())) && blockManager.isDisableBreak()) {
            event.setCancelled(true);
            return;
        }

        /** Called when the player does not have permission to break certain blocks in the world **/
        if (!player.hasPermission("inspiresurvival.block.break." + player.getWorld().getName())) {
            if (BlockConfig.getFile().get("blockMessage") != null && BlockConfig.getFile().get("notifyPlayer") != null && BlockConfig.getFile().getBoolean("notifyPlayer")) {
                new Message(BlockConfig.getFile().getString("blockMessage").replace("[action]", "break")).send(player, MessageType.HOTBAR);
            }
            event.setCancelled(true);
            return;
        }

        /** Called when the player is not holding the correct tool(s) to break the block **/
        if (!blockManager.getTools(block.getType()).contains(player.getItemInHand().getType())) {
            if (BlockConfig.getFile().get("toolMessage") != null && BlockConfig.getFile().get("notifyPlayer") != null && BlockConfig.getFile().getBoolean("notifyPlayer")) {
                new Message(BlockConfig.getFile().getString("toolMessage").replace("[block]", block.getType().toString())).send(player, MessageType.HOTBAR);
            }
            event.setCancelled(true);
            return;
        }

        /** Handle custom drops, set EXP and change the block type of it **/
        event.setCancelled(true);
        player.setExp(player.getExp() + blockManager.getExpToDrop(event.getExpToDrop()));

        event.getBlock().getDrops().clear();
        event.setExpToDrop(blockManager.getExpToDrop(event.getExpToDrop()));
        ItemStack randomItem = blockManager.getRandomItemToDrop(block.getType());

        if (randomItem.getType() != Material.AIR) {
            new Message("&bYou have procured &a+" + randomItem.getAmount() + " &6&l" + randomItem.getType().toString().toLowerCase().replaceAll("_", " ")).send(player, MessageType.HOTBAR);
        }
        player.getInventory().addItem(randomItem);
        player.setExp(player.getExp() + blockManager.getExpToDrop(event.getExpToDrop()));

        BlockManager.addBlock(block.getLocation());
        BlockManager.startBlockResetTimer(blockManager.getResetTime(event.getBlock().getType()), block.getLocation());

        block.setType(blockManager.getChangedType(block.getType()));
        displayCrack(player, block);
    }

    public void displayCrack(Player player, Block block){
        PacketPlayOutBlockBreakAnimation packet= new PacketPlayOutBlockBreakAnimation(0, new BlockPosition(block.getX(), block.getY(), block.getZ()), 8);
        int dimension = ((CraftWorld) player.getWorld()).getHandle().dimension;
        ((CraftServer) player.getServer()).getHandle().sendPacketNearby(block.getX(), block.getY(), block.getZ(), 12999, dimension, packet);
    }

}
