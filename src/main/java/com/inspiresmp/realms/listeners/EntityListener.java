package com.inspiresmp.realms.listeners;

import com.inspiresmp.realms.entity.EntityManager;
import com.inspiresmp.realms.utils.messages.Message;
import com.inspiresmp.realms.utils.messages.MessageType;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.ArrayList;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class EntityListener implements Listener {

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        Entity entity = event.getEntity();
        EntityManager entityManager = new EntityManager(entity);
        if (!(entity instanceof Player)) {
            if (entityManager.getRandomDrop() != null) {
                event.getDrops().clear();
                event.getDrops().add(entityManager.getRandomDrop());
            }
            event.setDroppedExp(entityManager.getExpToDrop(event.getDroppedExp()));
        }
    }

    @EventHandler
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event){
        if(!(event.getDamager() instanceof Player)) return;
        if(event.getEntity().getType() == EntityType.WITHER){
            Wither wither = (Wither) event.getEntity();
            if(event.getDamage() >= wither.getHealth()){
                ArrayList<CommandSender> onlinePlayers = new ArrayList<>();
                for(Player p : Bukkit.getOnlinePlayers()){
                    onlinePlayers.add(p);
                }
                new Message("&6&l" + ((Player)event.getDamager()).getName() + " &cdecimated the Wither Boss!").send(onlinePlayers, MessageType.HOTBAR);
            }
        }
    }

}
