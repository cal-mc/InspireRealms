package com.inspiresmp.realms.listeners;

import com.inspiresmp.realms.config.PlayerConfig;
import com.inspiresmp.realms.tokens.TokenManager;
import com.inspiresmp.realms.utils.WorldUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        PlayerConfig.getFile(event.getPlayer().getUniqueId());
        TokenManager.startTokenTimer(event.getPlayer().getUniqueId());
        event.getPlayer().teleport(WorldUtils.getSpawnLocation());
    }


}
