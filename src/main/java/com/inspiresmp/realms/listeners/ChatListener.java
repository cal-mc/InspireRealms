package com.inspiresmp.realms.listeners;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.config.TutorialConfig;
import com.inspiresmp.realms.realms.Realm;
import com.inspiresmp.realms.utils.BookUtils;
import com.inspiresmp.realms.utils.GameUtils;
import com.inspiresmp.realms.utils.PlayerUtils;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;


/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class ChatListener implements Listener {

    public static boolean enabled = true;
    public static Location location = null;

    @EventHandler
    public void onAuthenticationEvent(PlayerChatEvent event) {
        String message = event.getMessage();
        final Player player = event.getPlayer();
        if (PlayerUtils.authentication.containsKey(player.getUniqueId())) {
            event.setCancelled(true);
            if (message.equalsIgnoreCase(PlayerUtils.authentication.get(player.getUniqueId()).toString())) {
                Realm realm = new Realm(player);
                realm.purgeRealm();
                PlayerUtils.authentication.remove(player.getUniqueId());
                return;
            }
            PlayerUtils.sendMessage(player, "&cFailed to authenticate!");
            PlayerUtils.authentication.remove(player.getUniqueId());
        } else if(BookUtils.delete.containsKey(player.getUniqueId())){
            event.setCancelled(true);
            if(message.equalsIgnoreCase("yes")){
                BookUtils.deleteBookFromConfig(TutorialConfig.getFile(), BookUtils.delete.get(player.getUniqueId()), TutorialConfig.filePath);
                BookUtils.delete.remove(player.getUniqueId());
                return;
            }
            PlayerUtils.sendMessage(player, "&cFailed to authenticate!");
            BookUtils.delete.remove(player.getUniqueId());
        }  else if(message.equalsIgnoreCase("particle")){
            location = player.getLocation();
            new BukkitRunnable() {
                @Override
                public void run() {
                    if(enabled) {
                        GameUtils.makeSpiral(location, 60, 2F, 10 ,6);
                    }
                }
            }.runTaskTimerAsynchronously(Main.instance, 0L, 10L);

        } else if(message.equals("stop")){
            enabled = !enabled;
            if(location != null){
                location = null;
            }
        }
    }

}
