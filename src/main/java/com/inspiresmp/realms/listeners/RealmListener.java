package com.inspiresmp.realms.listeners;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.realms.Realm;
import com.inspiresmp.realms.realms.RealmGUI;
import com.inspiresmp.realms.realms.RealmManager;
import com.inspiresmp.realms.utils.WorldUtils;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class RealmListener implements Listener {

    @EventHandler
    public void onWorldChangeEvent(PlayerChangedWorldEvent event) {
        World fromWorld = event.getFrom();

        for (Realm realm : new ArrayList<>(RealmManager.getManager().getRealms())) {
            // We know we are dealing with a realm world
            if (realm.getRealmWorld() != null && realm.getRealmWorld().getName().equalsIgnoreCase(fromWorld.getName())) {
                if (realm.getPlayers().size() == 0) {
                    RealmManager.getManager().unloadRealm(realm);
                }
                return;
            }
        }
    }

    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        for (Realm realm : new ArrayList<>(RealmManager.getManager().getRealms())) {
            if (player.getWorld().getName().equalsIgnoreCase(player.getUniqueId().toString())) {
                if (realm.getPlayers().size() == 0) {
                    RealmManager.getManager().unloadRealm(realm);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerKickEvent(PlayerKickEvent event) {
        Player player = event.getPlayer();
        for (Realm realm : new ArrayList<>(RealmManager.getManager().getRealms())) {
            if (player.getWorld().getName().equalsIgnoreCase(player.getUniqueId().toString())) {
                if (realm.getPlayers().size() == 0) {
                    RealmManager.getManager().unloadRealm(realm);
                }
            }
        }
    }

    @EventHandler
    public void onInventoryClickEvent(InventoryClickEvent event) {
        if (!(event.getWhoClicked() instanceof Player) || event.getCurrentItem() == null) return;
        Player player = (Player) event.getWhoClicked();
        Inventory clickedInv = event.getClickedInventory();
        final String TITLE = event.getInventory().getTitle();
        if (!TITLE.equals(RealmGUI.UPGRADE_TITLE) || clickedInv != player.getInventory()) return;
        /** We know we are only dealing with the upgrade inventory BUT from the player's inventory point of view **/
        Realm realm = RealmManager.getManager().getPlayerRealm(player);

        List<ItemStack> items = realm.getRequiredResources();
        if (items == null) return;

        List<Material> requiredTypes = new ArrayList<>();
        for (ItemStack item : items) {
            requiredTypes.add(item.getType());
        }

        ItemStack item = event.getCurrentItem();
        if (item == null) return;

        Material type = item.getType();
        int amount = item.getAmount();
        if (!(clickedInv.contains(type) && requiredTypes.contains(type))) return;

        event.setCancelled(true);
        realm.addResource(type, amount);
    }

    @EventHandler
    public void onPlayerRespawnEvent(final PlayerRespawnEvent event){
        new BukkitRunnable() {
            public void run() {
                event.getPlayer().teleport(WorldUtils.getSpawnLocation());
            }
        }.runTaskLater(Main.instance, 1L);
    }

}

