package com.inspiresmp.realms.entity;

import com.inspiresmp.realms.Main;
import com.inspiresmp.realms.config.EntityConfig;
import com.inspiresmp.realms.menu.ItemFactory;
import com.inspiresmp.realms.realms.Realm;
import com.inspiresmp.realms.utils.PlayerUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.logging.Level;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class EntityManager {

    // Portal lists
    private static HashMap<Realm, List<Location>> portals = new HashMap<>();

    // The entity to get information for
    private Entity entity = null;
    // The type of the entity
    private EntityType type = null;

    public EntityManager(Entity entity) {
        this.entity = entity;
        this.type = entity.getType();
    }

    public static void addPortal(Realm realm, List<Location> locations){
        portals.put(realm,locations);
    }

    public static HashMap<Realm, List<Location>> getPortals(){
        return portals;
    }

    public static List<Location> getPortalLocations(Realm realm){
        if(hasPortal(realm)){
            return portals.get(realm);
        }
        return null;
    }

    public static void removePortal(Realm realm){
        if(!hasPortal(realm)){
            return;
        }
        portals.remove(realm);
    }

    public static boolean hasPortal(Realm realm){
        return portals.containsKey(realm);
    }

    public static void createPortal(Realm realm, Player player){
        if(hasPortal(realm)){
            PlayerUtils.sendMessage(player, "&cYour realm portal has already been placed!");
            return;
        }
    }

    /**
     * Gets a list of all items to be dropped when a entity dies
     *
     * @return List containing all of the drops
     */
    public List<ItemStack> getRawDrops() {
        YamlConfiguration entityConfig = EntityConfig.getFile();
        if (entityConfig.get(type.toString()) == null || entityConfig.get(type.toString() + ".drops.items") == null) {
            return null;
        }
        List<ItemStack> drops = new ArrayList<>();
        for (String itemString : entityConfig.getStringList(type.toString() + ".drops.items")) {
            try {
                String matIndex = ";";
                if (itemString.contains("#")) {
                    matIndex = "#";
                }
                Material material = Material.getMaterial(itemString.split(matIndex)[0]);
                int data = Integer.parseInt(itemString.split("#")[1].split(";")[0]);
                int min = Integer.parseInt(itemString.split(":")[1].split("-")[0]);
                int max = Integer.parseInt(itemString.split(":")[1].split("-")[1]);
                drops.add(new ItemFactory(material).setData(data).setAmount(Main.instance.random.nextInt((max - min) + 1) + min).getItem());
            } catch (Exception e) {
                Main.instance.getLogger().log(Level.WARNING, "Entity config error at: " + itemString);
            }
        }
        return drops;
    }

    /**
     * Gets a Map of all of the drops including there chance
     *
     * @return HashMap containing ItemStack and a chance int
     */
    public HashMap<ItemStack, Integer> getDrops() {
        YamlConfiguration entityConfig = EntityConfig.getFile();
        if (entityConfig.get(type.toString()) == null || entityConfig.get(type.toString() + ".drops.items") == null)
            return null;
        HashMap<ItemStack, Integer> drops = new HashMap<>();
        for (String itemString : entityConfig.getStringList(type.toString() + ".drops.items")) {
            try {
                String matIndex = ";";
                if (itemString.contains("#")) {
                    matIndex = "#";
                }
                int chance = Integer.parseInt(itemString.split(";")[1].split(":")[0]);
                Material material = Material.getMaterial(itemString.split(matIndex)[0]);
                int data = Integer.parseInt(itemString.split("#")[1].split(";")[0]);
                int min = Integer.parseInt(itemString.split(":")[1].split("-")[0]);
                int max = Integer.parseInt(itemString.split(":")[1].split("-")[1]);
                drops.put(new ItemFactory(material, Main.instance.random.nextInt((max - min) + 1) + min).setData(data).getItem(), chance);
            } catch (Exception e) {
                Main.instance.getLogger().log(Level.WARNING, "Entity config error at: " + itemString);
            }
        }
        return drops;
    }

    /**
     * Gets one random item
     * TODO: It works well but look at making it less resource intensive
     *
     * @return The random ItemStack
     */
    public ItemStack getRandomDrop() {
        if (getRawDrops() == null || getRawDrops().isEmpty()) return null;
        HashMap<ItemStack, Integer> drops = getDrops();
        int range = 0;
        for (int chance : drops.values()) {
            range = range + chance;
        }

        ItemStack item;
        List<ItemStack> items = new ArrayList<>();

        for (Map.Entry<ItemStack, Integer> map : drops.entrySet()) {
            item = map.getKey();
            int itemValue = map.getValue();
            for (int i = 0; i < itemValue; i++) {
                items.add(item);
            }
        }

        return items.get(Main.instance.random.nextInt(items.size()));
    }

    /**
     * Gets the amount of EXP to drop for a entity
     *
     * @param regularAmount The regular amount of EXP a entity drops
     * @return The amount of exp to drop
     */
    public int getExpToDrop(int regularAmount) {
        YamlConfiguration entityConfig = EntityConfig.getFile();
        try {
            return Integer.parseInt(entityConfig.getString(entity.getType().toString() + ".drops.exp"));
        } catch (Exception e) {
            return regularAmount;
        }
    }

}
